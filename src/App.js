import React, { useEffect, useState } from 'react';
import { Route, Switch } from "react-router-dom";
import { useDispatch } from 'react-redux';

import './App.css';
import './assets/css/vstyle.css';
import './assets/css/responsive.css';

import Navbar from './components/navbar';

import Template1 from './templates/template1';

import Home from './pages/home';
import BuildResume from './pages/buildResume/buildResume';
import CreateCoverLetter from './pages/createCoverLetter/createCoverLetter';
// import BeginnerTemplate1 from './components/buildResume/beginner/bt1-main'
import BeginnerTemplate1Init from './components/buildResume/beginner/bt1-init';
import CoverLetter1 from './components/createCoverLetter/cover-letter-1/cl1-init';
import SignUpModal from './components/signupModal';
import Profile from './pages/profile';
import VerifyAccount from './pages/verify-account'
import Paypal from './pages/paypal/paypal';
import ProtectedRoute from './components/protectedRoute';
import { saveIsPremium, saveIsVerified, signIn } from './actions';
import { isPremium, getCurrentUser, isVerified } from './services/user';
import { getUsername } from './services/encryption';
import Loader from 'react-loader';

function App() {

  const dispatch = useDispatch();
  const user = getCurrentUser();

  const [loaded, setLoaded] = useState(false);

  const init = async () => {

    const _isPremium = await isPremium()
    if (_isPremium) dispatch(saveIsPremium(true))

    const username = getUsername();
    if (username) dispatch(signIn());

    const _isVerified = await isVerified();
    if(_isVerified) dispatch(saveIsVerified(true));

    setLoaded(true);

  }

  useEffect(() => {
    init()
  },[])

  return (
    <Loader loaded={loaded} scale={1.0}>
      <SignUpModal />
      <Navbar />
      <Switch>
        {/* <Route path="/build-resume/preview" component={ResumePreviewPage}></Route> */}
        <ProtectedRoute path="/build-resume" component={BuildResume} />
        <ProtectedRoute path="/create-cover-letter" component={CreateCoverLetter} />
        {/* <Route path="/build-resume-beginner-template-1" component={BeginnerTemplate1}></Route> */}
        <ProtectedRoute path="/build-resume-beginner-template-1" component={BeginnerTemplate1Init} />
        <ProtectedRoute path="/create-cover-letter-cover-letter-1" component={CoverLetter1} />
        <ProtectedRoute path="/template1" component={Template1} />
        <ProtectedRoute path="/membership" component={Paypal} />
        {/* {user && <Route path="/profile" component={Profile} />}
        {user && <Route path="/verify-account" component={VerifyAccount} />} */}
        {<Route path="/profile" component={Profile} />}
        {<Route path="/verify-account" component={VerifyAccount} />}
        <Route path="/" component={Home}></Route>
      </Switch>
    </Loader>
  );
}

export default App;
