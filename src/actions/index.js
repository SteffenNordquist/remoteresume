export const signIn = () => {
    return { type: 'SIGN_IN' }
}

export const signOut = () => {
    return { type: 'SIGN_OUT' }
}

export const saveIsPremium = (isPremium) => {
    return { type: 'IS_PREMIUM', isPremium }
}

export const saveIsVerified = (isVerified) => {
    return { type: 'IS_VERIFIED', isVerified }
}