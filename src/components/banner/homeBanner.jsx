import React from 'react';
import { useSelector } from 'react-redux';
import LinkButton from '../linkButton';

const HomeBanner = () => {
    const state = useSelector(state => state);

    const renderSigninUser = () => {
        return <>
            {state.verifiedUser ?
                renderPremiumButtons()
                :
                <>
                    <LinkButton className="btn btn-primary btn-lg" to="/verify-account">Verify Account</LinkButton>
                </>
            }
        </>
    }

    const renderPremiumButtons = () => {
        return <>
        {
            state.premiumUser ?
                <div style={{ display: 'flex' }}>
                    <LinkButton className="btn btn-primary btn-lg" to="/build-resume">Build My Resume</LinkButton>
                    <p className="ml-3 mr-2 mt-3">or</p>
                    <LinkButton className="btn btn-primary btn-lg" to="/create-cover-letter">Create My Cover Letter</LinkButton>
                </div> 
            : 
                <LinkButton className="btn btn-primary btn-lg" to="/profile">
                    Pay Now To Get Started!
                </LinkButton>
            }
        </>
    }

    const renderSignupNowButton = () => {
        return <>
            <button className="btn btn-primary mr-2" data-toggle="modal" data-target="#signup-modal">Sign Up Now!</button>
        </>
    }

    return (
        <div className="jumbotron">
            <div className="container">
                <h1 className="display-4">Creating Resume, <br /> Becomes <span className="text-primary">Easier</span></h1>
                <p className="lead mb-5">Provides easy and hassle-free way in creating your resume.</p>
                {state.user ? renderSigninUser() : renderSignupNowButton()}
            </div>
        </div>
    );
};

export default HomeBanner;