import React from 'react';
import { useHistory } from "react-router-dom";

const BeginnerTemplates = (props) => {

    const {routePrefix} = props;

    let history = useHistory();

    const updateTemplate = (template) => {

        // var resumeData = JSON.parse(localStorage.getItem("resumeData"));
        // resumeData.template = template;
        // localStorage.setItem("resumeData", JSON.stringify(resumeData));

        const newRoute = routePrefix + "-beginner-" + template;
        history.push(newRoute);
    }

    return (
        <div className="row w-100">
            <div className="col">

                <div className="row text-center">
                    <div className="col">
                        <div className="intro-text my-5">
                            <h1>Choose a resume template!</h1>
                            <p className="lead">Some additional text about this part</p>
                        </div>
                    </div>
                </div>
                
                <div className="row options text-center">
                    <div className="col-sm-6">
                        <div className="box shadow" onClick={() => updateTemplate('template-1')}>
                            <h3>Template 1</h3>
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="box shadow" onClick={() => /*updateTemplate('template2')*/ alert("template under construction")}>
                            <h3>Template 2</h3>
                        </div>
                    </div>
                    {/* <div className="col-sm-4">
                        <div className="box shadow" onClick={() => updateTemplate('template3')}>
                            <h3>Template 3</h3>
                        </div>
                    </div> */}
                </div>

                <hr className="mt-5" />

                <div className="row">
                    <div className="col">
                        <button className="btn btn-outline-dark px-4" onClick={() => history.push(routePrefix + "/experience-level")}>Back</button>
                    </div>
                </div>

            </div>
        </div>
    );
};

export default BeginnerTemplates;