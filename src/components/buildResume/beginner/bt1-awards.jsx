import React, { useState, useContext } from 'react';
import { useHistory } from "react-router-dom";
import ResumeContext from '../../../providers/resume-context';

const AwardsSection = (props) => {

    const {resumeData, saveResumeData} = useContext(ResumeContext);

    const [heading, setHeading] = useState(resumeData.awards.heading ? resumeData.awards.heading : "organizations");

    const [organizations, setOrganizations] = useState(resumeData.awards.organizations ? resumeData.awards.organizations : []);
    const [organizationToAdd, setOrganizationToAdd] = useState("");

    const [awards, setAwards] = useState(resumeData.awards.awards ? resumeData.awards.awards : []);
    const [awardToAdd, setAwardToAdd] = useState("");

    const {routePrefix} = props;

    let history = useHistory();

    const addItem = () => {

        if (heading === "organizations") {
            setOrganizations([...organizations, organizationToAdd]);
            setOrganizationToAdd("");
        } else {
            setAwards([...awards, awardToAdd]);
            setAwardToAdd("");
        }
    };

    const changeHeading = (value) => {
        setHeading(value);
    };

    const keyPressedOnAwardsAdding = (event) => {
        if (event.key === 'Enter') {
            addItem();
        }
    }

    const removeItem = (index) => {

        if (heading === "organizations") {
            organizations.splice(index, 1);

            setOrganizations([...organizations]);
        } else {
            awards.splice(index, 1);

            setAwards([...awards]);
        }
        
    };

    const setItemToAdd = (value) => {

        if (heading === "organizations")
            setOrganizationToAdd(value);
        else
            setAwardToAdd(value);
    };

    const updateAwards = () => {
        
        resumeData.awards = {
            heading: heading,
            awards: awardToAdd ? [...awards, awardToAdd] : [...awards],
            organizations: organizationToAdd ? [...organizations, organizationToAdd] : [...organizations],
        };
        
        saveResumeData("beginner","template1", resumeData);
        
        history.push(routePrefix + "/job-experience");
    };

    const updateDefaultAwards = (input, index) => {

        awards[index] = input;
        setAwards(awards);
    };

    const updateItems = (input, index) => {

        if (heading === "organizations") {
            organizations[index] = input;
            setOrganizations(organizations);
        } else {
            awards[index] = input;
            setAwards(awards);
        }
    };

    return (
        <>
            <div className="row w-100">
            <div className="col">

                <div className="row">
                    <div className="col">
                        
                        <div className="intro-text my-5">
                            {/* <h1>Organizations / Awards / Accomplishments</h1> */}

                            <p>Select Section Heading</p>
                            <div className="btn-group btn-group-lg mb-3" role="group">
                                <button className={`btn btn-outline-dark ${heading === "organizations" ? "active" : ""}`} onClick={() => changeHeading("organizations")}>Organizations</button>
                                <button className={`btn btn-outline-dark ${heading === "awards / accomplishments" ? "active" : ""}`} onClick={() => changeHeading("awards / accomplishments")}>Awards/Accomplishments</button>
                            </div>
                        </div>

                        {/* <div className="input-repeater">
                            {items.map((item, index) => (
                            <div className="row" key={"item"+index}>
                                <div className="col">
                                    <div className="form-group">
                                        <p className="action-remove text-right mb-1" onClick={() => removeAward(index)}><i className="fa fa-trash-o" aria-hidden="true"></i></p>
                                        <input type="text" name={"item" + index} id={"item" + index} className="form-control pr-5" defaultValue={item}
                                            placeholder=""
                                            onChange={(input) => updateItems(input.target.value, index)} />
                                    </div>
                                </div>
                            </div>
                            ))}
                        </div> */}

                        {heading == "organizations" ?
                        <div className="input-repeater">
                            {organizations.map((org, index) => (
                            <div className="row" key={"org"+index}>
                                <div className="col">
                                    <div className="form-group">
                                        <p className="action-remove text-right mb-1" onClick={() => removeItem(index)}><i className="fa fa-trash-o" aria-hidden="true"></i></p>
                                        <input type="text" name={"org" + index} id={"org" + index} className="form-control pr-5" value={org}
                                            placeholder=""
                                            onChange={(input) => updateItems(input.target.value, index)} />
                                    </div>
                                </div>
                            </div>
                            ))}
                        </div>
                        :
                        <div className="input-repeater">
                            {awards.map((award, index) => (
                            <div className="row" key={"award"+index}>
                                <div className="col">
                                    <div className="form-group">
                                        <p className="action-remove text-right mb-1" onClick={() => removeItem(index)}><i className="fa fa-trash-o" aria-hidden="true"></i></p>
                                        <input type="text" name={"awards" + index} id={"awards" + index} className="form-control pr-5" value={award}
                                            placeholder=""
                                            onChange={(input) => updateItems(input.target.value, index)} />
                                    </div>
                                </div>
                            </div>
                            ))}
                        </div>
                        }

                        <div className="row">
                            <div className="col">
                                <div className="form-group">
                                    <label htmlFor="awardtoadd" className="control-label">Add here...</label>
                                    <input type="text" name="awardtoadd" id="awardtoadd" className="form-control"
                                        placeholder=""
                                        value={heading == "organizations" ? organizationToAdd : awardToAdd}
                                        onChange={(input) => setItemToAdd(input.target.value)} 
                                        onKeyPress={(event) => keyPressedOnAwardsAdding(event)}/>
                                </div>
                            </div>
                        </div>

                        <div className="row mt-3">
                            <div className="col text-center">
                                <button className="btn btn-outline-primary btn-sm" onClick={() => addItem()}><i className="fa fa-plus" aria-hidden="true"></i> Add Another</button>
                            </div>
                        </div>

                        <hr className="mt-5" />

                        <div className="row">
                            <div className="col">
                                <button className="btn btn-outline-dark px-4" onClick={() => history.push(routePrefix + "/education")}>Back</button>
                            </div>
                            <div className="col text-right">
                                <button className="btn btn-primary px-4" onClick={() => updateAwards()}>Next</button>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
         </>
    );
};

export default AwardsSection;