import React, { useState, useContext } from 'react';
import { useHistory } from "react-router-dom";
import ResumeContext from '../../../providers/resume-context';

const ContactSection = (props) => {

    const {resumeData, saveResumeData} = useContext(ResumeContext);
    
    const [firstname, setFirstname] = useState(resumeData.contact.firstname);
    const [lastname, setLastname] = useState(resumeData.contact.lastname);
    const [city, setCity] = useState(resumeData.contact.city);
    const [state, setState] = useState(resumeData.contact.state);
    const [zipcode, setZipcode] = useState(resumeData.contact.zipcode);
    const [phone, setPhone] = useState(resumeData.contact.phone);
    const [email, setEmail] = useState(resumeData.contact.email);
    const [linkedIn, setLinkedIn] = useState(resumeData.contact.linkedIn);

    const [fnameErr, setFnameErr] = useState('');
    const [lnameErr, setLnameErr] = useState('');


    const {routePrefix} = props;

    let history = useHistory();

    const updateContact = () => {
        
        updateInputErrors();

        if(!canGoNext())
        {
            return;
        }
        
        resumeData.contact = {
            firstname: firstname,
            lastname: lastname,
            city: city,
            state: state,
            zipcode: zipcode,
            phone: phone,
            email: email,
            linkedIn: linkedIn
        };
        
        saveResumeData("beginner","template1", resumeData);

        history.push(routePrefix + "/objective");
    }

    const canGoNext = () => {
        if(firstname && lastname){
            return true;
        }
        return false;
    }

    const updateInputErrors = () =>{
        
        (function(){!firstname ? setFnameErr("Firstname is required") : setFnameErr("")})();
        (function(){!lastname ? setLnameErr("Lastname is required") : setLnameErr("")})();
            
    }

    return (
        <>
            <div className="row w-100">
            <div className="col">

                <div className="row">
                    <div className="col">
                        <div className="intro-text my-5">
                            <h2>What’s the best way for employers to contact you?</h2>
                            <p className="lead">We suggest including an email and phone number.</p>
                        </div>

                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="firstname" className="control-label">Firstname</label>
                                    <input type="text" name="firstname" id="firstname" className="form-control" 
                                        defaultValue={firstname} onChange={(input) => setFirstname(input.target.value)} />
                                    <small className="form-text text-danger">{fnameErr}</small>
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="lastname" className="control-label">Lastname</label>
                                    <input type="text" name="lastname" id="lastname" className="form-control" 
                                        defaultValue={lastname} onChange={(input) => setLastname(input.target.value)} />
                                    <small className="form-text text-danger">{lnameErr}</small>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-sm-4">
                                <div className="form-group">
                                    <label htmlFor="city" className="control-label">City</label>
                                    <input type="text" name="city" id="city" className="form-control"
                                        defaultValue={city} onChange={(input) => setCity(input.target.value)} />
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="form-group">
                                    <label htmlFor="state" className="control-label">State/Province</label>
                                    <input type="text" name="state" id="state" className="form-control"
                                        defaultValue={state} onChange={(input) => setState(input.target.value)} />
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="form-group">
                                    <label htmlFor="zipcode" className="control-label">Zip Code</label>
                                    <input type="text" name="zipcode" id="zipcode" className="form-control"
                                        defaultValue={zipcode} onChange={(input) => setZipcode(input.target.value)} />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-sm-4">
                                <div className="form-group">
                                    <label htmlFor="phone" className="control-label">Phone</label>
                                    <input type="text" name="phone" id="phone" className="form-control"
                                        defaultValue={phone} onChange={(input) => setPhone(input.target.value)}/>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="form-group">
                                    <label htmlFor="email" className="control-label">Email Address</label>
                                    <input type="email" name="email" id="email" className="form-control"
                                        defaultValue={email} onChange={(input) => setEmail(input.target.value)}/>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="form-group">
                                    <label htmlFor="linkedIn" className="control-label">LinkedIn Profile</label>
                                    <input type="text" name="linkedIn" id="linkedIn" className="form-control"
                                        defaultValue={linkedIn} onChange={(input) => setLinkedIn(input.target.value)}/>
                                </div>
                            </div>
                        </div>

                        <hr className="mt-5" />

                        <div className="row">
                            <div className="col">
                                <button className="btn btn-outline-dark px-4" onClick={() => history.push("../build-resume/beginner-templates")}>Back</button>
                            </div>
                            <div className="col text-right">
                                <button className="btn btn-primary px-4" onClick={() => updateContact()}>Next</button>
                            </div>
                        </div>
                        

                    </div>
                </div>

            </div>
            </div>
        </>
    );
};

export default ContactSection;