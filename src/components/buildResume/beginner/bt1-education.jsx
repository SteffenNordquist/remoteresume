import React, { useState, useContext } from 'react';
import { useHistory } from "react-router-dom";
import EducationModal from "../educationModal";
import ResumeContext from '../../../providers/resume-context';
import $ from 'jquery';
import Calendar from 'react-calendar';

const EducationSection = (props) => {

    const {resumeData, saveResumeData} = useContext(ResumeContext);

    const [educations, setEducations] = useState(resumeData.educations ? resumeData.educations : []);
    // const [educationToEdit, setEducationToEdit] = useState({});
    // const [educationToEditIndex, setEducationToEditIndex] = useState({});

    // const [errors, setErrors] = useState([]);
    const [go, setGo] = useState(false);

    const [school, setSchool] = useState("");
    const [degree, setDegree] = useState("");
    const [studyField, setStudyField] = useState("");
    const [city, setCity] = useState("");
    const [state, setState] = useState("");
    const [graduateDate, setGraduateDate] = useState("");
    const [classes, setClasses] = useState([]);
    const [classToAdd, setClassToAdd] = useState("");

    const [degreeErr, setDegreeErr] = useState("");
    const [studyErr, setStudyErr] = useState("");
    const [cityErr, setCityErr] = useState("");
    const [stateErr, setStateErr] = useState("");
    const [graduateDateErr, setGraduateDateErr] = useState("");

    const degrees = ["High School Diploma","Associate of Arts",
    "Associate of Science","Bachelor of Arts","Bachelor of Science",
    "Master of Arts","Master of Science","M.D.","Ph.D."];

    let history = useHistory();

    const {routePrefix} = props;

    // const editDefaultEducation = (education, index) => {
    //     setEducationToEdit(education);
    //     setEducationToEditIndex(index);
    //     $('#education-modal-'+index).modal('show');
    // };

    const addClass = () => {
        if (classToAdd) {
            setClasses([...classes, classToAdd]);
            setClassToAdd("");
        }
    };

    const addEducation = () => {

        const educationToAdd = {
            school: school,
            degree: degree,
            studyField: studyField,
            city: city,
            state: state,
            graduateDate: graduateDate,
            classes: classToAdd ? [...classes, classToAdd] : [...classes]
        };

        updateValidationErrors();
        if(!canGoNext())
        {
            return;
        }

        if (school) {

            setEducations([...educations, educationToAdd]);
            setSchool("");
            setDegree("college");
            setCity("");
            setState("");
            setGraduateDate("");
            setClasses([]);
        }
    };

    const canGoNext = () => {

        let isStudyFieldGood = studyField || degree === "High School Diploma";

        if(!school)
            return true;

        if(degree && isStudyFieldGood && city && state && graduateDate){
            return true;
        }
        return false;
    }

    const closeCalender = (calendarSelector) => {
        $('.react-calendar.'+ calendarSelector).css({"opacity":"0","visibility":"hidden"});
    };

    const handleChange = (event, setValue) => {
        setValue(event.target.value)
    };

    const hasErrors = () => {
        return $('.error-text').length > 0 ? true : false;
    };

    const keyPressedOnClassAdding = (event) => {
        if (event.key === 'Enter') {
            addClass();
        }
    }

    const openCalendar = (calendarSelector) => {
        $('.react-calendar.'+ calendarSelector).css({"opacity":"1","visibility":"visible"});
    };

    const removeClass = (index) => {
        classes.splice(index, 1);
        setClasses([...classes]);
    };

    const removeEducation = (index) => {
        educations.splice(index, 1);
        setEducations([...educations]);
    };

    const setCalendar = (date, calendarSelector, setField) => {

        setField(date.toLocaleDateString("en-US", {
            month: "short",
            year: "numeric"
        }));

        closeCalender(calendarSelector);
    };

    const update = (education, index) => {

        educations[index] = education;
        setEducations([...educations]);
    };

    const updateDefaultClasses = (input, index) => {
        classes[index] = input;
        setClasses(classes);
    };

    const updateEducation = () => {

        updateValidationErrors();

        if(!canGoNext())
        {
            return;
        }

        setGo(true);

        // TODO: Refactor this -> Check if has error first. Do this in a ReactJS Way
        setTimeout(function() {
            if ((hasErrors() && educations.length === 0))
                return;
    
            const educationToAdd = {
                school: school,
                degree: degree ? degree : "unselected",
                studyField: studyField,
                city: city,
                state: state,
                graduateDate: graduateDate,
                classes: classToAdd ? [...classes, classToAdd] : [...classes]
            };
    
            resumeData.educations = educationToAdd.school !== "" ? [...educations, educationToAdd] : [...educations];
    
            saveResumeData("beginner","template1", resumeData);

            history.push(routePrefix + "/awards");

        }, 100);
        
    };

    const updateValidationErrors = () =>{
        
        let isStudyFieldGood = studyField || degree === "High School Diploma";

        if(school){            
            (function(){!degree ? setDegreeErr("Degree is required") : setDegreeErr("")})();
            //(function(){degree !== "High School Diploma" ? setStudyErr("Field of study is required") : setStudyErr("")})();
            (function(){isStudyFieldGood ? setStudyErr() : setStudyErr("")})();
            (function(){!city ? setCityErr("City is required") : setCityErr("")})();
            (function(){!state ? setStateErr("State is required") : setStateErr("")})();
            (function(){!graduateDate ? setGraduateDateErr("Graduation date is required") : setGraduateDateErr("")})();
        }
            
    }

    return (
        <>
            <div className="row w-100">
            <div className="col">

                <div className="row">
                    <div className="col">
                        <div className="intro-text my-5">
                            <h1>Education</h1>
                            <p className="lead">Tell us about your educational background.</p>
                        </div>

                        <div className="input-repeater">
                            {educations.map((education, index) => (

                            <div className="box box-1 mb-2" key={index}>
                                <EducationModal education={education} index={index} update={update} />

                                <div className="row">
                                    <div className="col">
                                        <p className="action-remove">
                                            {/* <i className="fa fa-pencil mr-2" onClick={() => editDefaultEducation(education, index)} aria-hidden="true"></i> */}
                                            <i className="fa fa-trash-o" onClick={() => removeEducation(index)} aria-hidden="true"></i>
                                        </p>
                                        <h4 className="text-center">{education.degree} 
                                            {education.studyField &&
                                                <span> in {education.studyField}</span>
                                            }
                                        </h4>
                                        <hr className="mb-2" />
                                        {education.school &&
                                            <p className="text-center text-muted">@ {education.school}</p>
                                        }
                                    </div>
                                </div>

                            </div>
                            ))}
                        </div>

                        <div className="box box-2 mt-5">
                            <h4>Add Education</h4>
                            <div className="row">
                                <div className="col">
                                    <div className="form-group">
                                        <label htmlFor="school" className="control-label">School Name</label>
                                        <input type="text" name="school" id="school" className="form-control"
                                            value={school} onChange={(event) => handleChange(event, setSchool)} />
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="degree" className="control-label">Degree</label>
                                        <select value={degree} className="form-control" name="degree" id="degree" onChange={event => handleChange(event, setDegree)}>

                                            <option value="">Select</option>
                                            {degrees.map((degree, index) => (
                                                <option key={index} value={degree}>{degree}</option>
                                            ))}
                                        </select>
                                        <small className="form-text text-danger">{degreeErr}</small>
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="field" className="control-label">Field of Study</label>
                                        <input type="text" name="field" id="field" className="form-control" 
                                            disabled={(degree === "High School Diploma")} value={studyField} onChange={(event) => handleChange(event, setStudyField)} />
                                        <small className="form-text text-danger">{studyErr}</small>
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="city" className="control-label">City</label>
                                        <input type="text" name="city" id="city" className="form-control"
                                            value={city} onChange={(input) => setCity(input.target.value)} />
                                        <small className="form-text text-danger">{cityErr}</small>
                                    </div>
                                </div>
                                <div className="col-sm-3">
                                    <div className="form-group">
                                        <label htmlFor="state" className="control-label">State</label>
                                        <input type="text" name="state" id="state" className="form-control"
                                            value={state} onChange={(input) => setState(input.target.value)} />
                                        <small className="form-text text-danger">{stateErr}</small>
                                    </div>
                                </div>
                                <div className="col-sm-3">
                                    <div className="form-group">
                                        <label htmlFor="graduate-date" className="control-label">Graduation Date</label>
                                        <div className="input-group mb-3 input-group">
                                            <div className="input-group-prepend">
                                            <span className="input-group-text"><i className="fa fa-calendar" aria-hidden="true"></i></span>
                                            </div>
                                            <input type="text" name="graduate-date" id="graduate-date" className="form-control" value={graduateDate}
                                                onFocus={() => openCalendar('graduate-date')} readOnly
                                                />
                                            <small className="form-text text-danger">{graduateDateErr}</small>
                                        </div>
                                    </div>
                                    <Calendar 
                                        className="datepicker graduate-date"
                                        defaultView="year" 
                                        view="year" 
                                        prev2Label={null} 
                                        next2Label={null} 
                                        maxDetail="year"
                                        minDetail="decade"
                                        formatMonth={(locale, date) => date.toLocaleDateString("en-US", {month: 'short'})} 
                                        onChange={(date) => setCalendar(date, "graduate-date", setGraduateDate)}
                                        />
                                </div>
                            </div>

                            <div className="box mt-1">
                                <div className="row">
                                    <div className="col">
                                        <h5>Classes</h5>

                                        <div className="input-repeater">
                                            {classes && classes.map((schoolClass, index) => (
                                            <div className="row" key={Math.random()}>
                                                <div className="col">
                                                    <div className="form-group">
                                                        <p className="action-remove text-right mb-1" onClick={() => removeClass(index)}><i className="fa fa-trash-o" aria-hidden="true"></i></p>
                                                        {/* <label htmlFor={"awards" + index} className="control-label">Organizations / Awards / Accomplishments</label> */}
                                                        <input type="text" name={"school-class" + index} id={"school-class" + index} className="form-control pr-5" value={schoolClass}
                                                            placeholder=""
                                                            onChange={(input) => updateDefaultClasses(input.target.value, index)} />
                                                    </div>
                                                </div>
                                            </div>
                                            ))}
                                        </div>

                                        <div className="row">
                                            <div className="col">
                                                <div className="form-group">
                                                    <input type="text" name="classtoadd" id="classtoadd" className="form-control"
                                                        placeholder="Add class name"
                                                        value={classToAdd}
                                                        onChange={(input) => setClassToAdd(input.target.value)} 
                                                        onKeyPress={(event) => keyPressedOnClassAdding(event)}/>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col">
                                        <button className="btn btn-outline-default btn-sm" onClick={() => addClass()}><i className="fa fa-plus" aria-hidden="true"></i> Add Another Class</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row mt-4">
                            <div className="col text-center">
                                <button className="btn btn-outline-primary btn-sm" onClick={() => addEducation()}><i className="fa fa-plus" aria-hidden="true"></i> Add Another</button>
                            </div>
                        </div>

                        <hr className="mt-5" />

                        <div className="row">
                            <div className="col">
                                <button className="btn btn-outline-dark px-4" onClick={() => history.push(routePrefix + "/objective")}>Back</button>
                            </div>
                            <div className="col text-right">
                                <button className="btn btn-primary px-4" onClick={() => updateEducation()}>Next</button>
                            </div>
                        </div>
                        

                    </div>
                </div>

            </div>
        </div>
        </>
    );
};

export default EducationSection;