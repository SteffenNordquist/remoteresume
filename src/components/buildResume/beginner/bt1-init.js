import React, { useEffect, useState } from 'react';
import { getResumeData, defaultResume } from '../../../services/persistence';
import Bt1Main from './bt1-main';
import { finalizedResumeFromSource } from '../../../utils/resumeFinalizerFromSource';
import Loader from 'react-loader';

const Bt1Init = () => {

    useEffect(() => {
        initLoad();
    }, []);

    const initLoad = async () => {

        let retries = 0;
        while (true) {
            try {
                const loadedResume = await getResumeData("beginner", "template1");
                const finalResumeData = finalizedResumeFromSource(loadedResume);
                setResumeData(finalResumeData);
                setResumeLoaded(true);
                break;
            } catch (err) {
                retries++
                if (retries > 3) {
                    alert("Error occured, please try again later.");
                    break;
                }
            }
        }
    }

    const [resumeLoaded, setResumeLoaded] = useState(false);
    const [resumeData, setResumeData] = useState(defaultResume());

    return (
        <Loader loaded={resumeLoaded} scale={1.0}>
            <Bt1Main loadedResumeData={resumeData} />
        </Loader>
    );

};

export default Bt1Init;