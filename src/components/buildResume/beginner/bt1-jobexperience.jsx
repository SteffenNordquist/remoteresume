import React, { useState, useEffect, useContext } from 'react';
import { useHistory } from "react-router-dom";
// import JobExperienceModal from "../jobExperienceModal";
import ResumeContext from '../../../providers/resume-context';
import $ from 'jquery';
import '../../buildResume/buildResume.css';
import Calendar from 'react-calendar';
import { getJobTitleAndDuties } from '../../../services/jobTitleAndDuties';

const JobExperienceSection = (props) => {
    const { resumeData, saveResumeData } = useContext(ResumeContext);

    const [selectedJobDuties, setSelectedJobDuties] = useState([]);

    const [jobExperiences, setJobExperiences] = useState(resumeData.jobExperiences ? resumeData.jobExperiences : []);
    // const [jobExperienceToEdit, setJobExperienceToEdit] = useState({});
    // const [jobExperienceToEditIndex, setJobExperienceToEditIndex] = useState({});

    const [jobTitle, setJobTitle] = useState("");
    const [employer, setEmployer] = useState("");
    const [city, setCity] = useState("");
    const [state, setState] = useState("");
    const [employmentStart, setEmploymentStart] = useState("");
    const [employmentEnd, setEmploymentEnd] = useState("");
    const [isPresentWork, setIsPresentWork] = useState(false);
    const [description, setDescription] = useState("");
    const [duties, setDuties] = useState([]);
    const [dutyToAdd, setDutyToAdd] = useState("");
    const [jobTitleDuties, setJobTitleDuties] = useState([])
    const [customDuty, setCustomDuty] = useState('')

    const { routePrefix } = props;
    let history = useHistory();

    const [jobTitleErr, setJobTitleErr] = useState("");
    const [employerErr, setEmployerErr] = useState("");
    const [cityErr, setCityErr] = useState("");
    const [stateErr, setStateErr] = useState("");
    const [employmentStartErr, setEmploymentStartErr] = useState("");
    const [employmentEndErr, setEmploymentEndErr] = useState("");

    const fetch = async () => {
        const { data } = await getJobTitleAndDuties()

        let _jobTitlesDuties = []
        data.map(({ job_title, duties }) => {
            let _jobTitleDuties = {
                jobTitle: job_title,
                duties: duties.split('||')
            }
            _jobTitlesDuties.push(_jobTitleDuties)
        })
        setJobTitleDuties(_jobTitlesDuties)
    }

    useEffect(() => {
        fetch()
    }, [])

    // const update = (experience, index) => {

    //     jobExperiences[index] = experience;
    //     setJobExperiences([...jobExperiences]);
    // };

    // const editDefaultJobExperience = (jobExperience, index) => {

    //     setJobExperienceToEdit(jobExperience);
    //     setJobExperienceToEditIndex(index);
    //     $('#job-experience-modal-'+index).modal('show');
    // };

    const addDuty = () => {
        setDuties([...duties, dutyToAdd || customDuty]);
        setCustomDuty('')
        setDutyToAdd('');
    };

    const addjobExperience = () => {

        const newDutyToAdd = dutyToAdd || customDuty;

        const jobExperienceToAdd = {
            jobTitle: jobTitle,
            employer: employer,
            city: city,
            state: state,
            employmentStart: employmentStart,
            employmentEnd: employmentEnd,
            description: description,
            duties: newDutyToAdd ? [...duties, newDutyToAdd] : [...duties]
        };

        updateValidationErrors();
        if (!canGoNext()) {
            return;
        }

        if (jobTitle && employer) {
            setJobExperiences([...jobExperiences, jobExperienceToAdd]);
            setJobTitle("");
            setEmployer("");
            setCity("");
            setState("");
            setEmploymentStart("");
            setEmploymentEnd("");
            setIsPresentWork(false);
            setDescription("");
            setDuties([]);
        }
    };

    const canGoNext = () => {

        if (!jobTitle && !employer)
            return true;

        let canGo = false;
        if (jobTitle && employer && city && state && employmentStart) {
            canGo = true;
        }
        if (!isPresentWork && !employmentEnd) {
            canGo = false;
        }

        return canGo;
    }

    const closeCalender = (calendarSelector) => {
        $('.react-calendar.' + calendarSelector).css({ "opacity": "0", "visibility": "hidden" });
    };

    const customDutyEntered = (event) =>{
        if(event.key === 'Enter'){
            addDuty();
        }
    }

    const jobTitleChanged = (event, setJobTitle) => {
        setJobTitle(event.target.value)
        const matchedJobDuties = jobTitleDuties.find(x => x.jobTitle === event.target.value);
        if (matchedJobDuties)
            setSelectedJobDuties(matchedJobDuties.duties);
    };

    const openCalendar = (calendarSelector) => {
        $('.react-calendar.' + calendarSelector).css({ "opacity": "1", "visibility": "visible" });
    };

    const removeDuty = (index) => {
        duties.splice(index, 1);
        setDuties([...duties]);
    };

    const removejobExperience = (index) => {
        jobExperiences.splice(index, 1);
        setJobExperiences([...jobExperiences]);
    };

    const setCalendar = (date, calendarSelector, setField) => {

        setField(date.toLocaleDateString("en-US", {
            month: "short",
            year: "numeric"
        }));

        closeCalender(calendarSelector);
    };

    const togglePresentWork = (event) => {
        const checked = event.target.checked;
        if (checked) {
            setEmploymentEnd("Present");
            setIsPresentWork(checked);
        } else {
            setEmploymentEnd("");
            setIsPresentWork(checked);
        }
    };

    const updateDefaultDuties = (input, index) => {
        duties[index] = input;
        setDuties(duties);
    };

    const updateJobExperience = () => {

        const newDutyToAdd = dutyToAdd || customDuty;

        const jobExperienceToAdd = {
            jobTitle: jobTitle,
            employer: employer,
            city: city,
            state: state,
            employmentStart: employmentStart,
            employmentEnd: employmentEnd,
            description: description,
            duties: newDutyToAdd ? [...duties, newDutyToAdd] : [...duties]
        };

        updateValidationErrors();
        if (!canGoNext()) {
            return;
        }

        resumeData.jobExperiences = jobExperienceToAdd.jobTitle !== "" ? [...jobExperiences, jobExperienceToAdd] : [...jobExperiences];
        saveResumeData("beginner", "template1", resumeData);

        history.push(routePrefix + "/preview");
    }

    const updateValidationErrors = () => {

        if (jobTitle || employer) {
            (function () { !jobTitle ? setJobTitleErr("Job Title is required") : setJobTitleErr("") })();
            (function () { !employer ? setEmployerErr("Employer is required") : setEmployerErr("") })();
            (function () { !city ? setCityErr("City is required") : setCityErr("") })();
            (function () { !state ? setStateErr("State is required") : setStateErr("") })();
            (function () { !employmentStart ? setEmploymentStartErr("Start date is required") : setEmploymentStartErr("") })();
            (function () { !employmentEnd ? setEmploymentEndErr("End date is required") : setEmploymentEndErr("") })();
        }
        else {
            setJobTitleErr("");
            setEmployerErr("");
            setCityErr("");
            setStateErr("");
            setEmploymentStartErr("");
            setEmploymentEndErr("");
        }
    }

    return (
        <>
            <div className="row w-100">
                <div className="col">

                    <div className="row">
                        <div className="col">
                            <div className="intro-text my-5">
                                <h1>Tell us about your work experience</h1>
                                <p className="lead">Start with your most recent job and work backward.</p>
                            </div>

                            <div className="input-repeater">
                                {jobExperiences.map((jobExperience, index) => (

                                    <div className="box box-1 mb-4" key={index}>
                                        {/* <JobExperienceModal jobExperience={jobExperience} index={index} update={update} /> */}
                                        <div className="row">
                                            <div className="col">
                                                <p className="action-remove">
                                                    {/* <i className="fa fa-pencil mr-2" onClick={() => editDefaultJobExperience(jobExperience, index)} aria-hidden="true"></i> */}
                                                    <i className="fa fa-trash-o" onClick={() => removejobExperience(index)} aria-hidden="true"></i>
                                                </p>
                                                <h4 className="text-center">{jobExperience.jobTitle} @ {jobExperience.employer}</h4>
                                                <hr />
                                            </div>
                                        </div>

                                    </div>
                                ))}
                            </div>

                            <div className="mt-5">
                                <h4>Add Experience</h4>
                                <div className="row">
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <label htmlFor="jobTitle" className="control-label">Job Title</label>
                                            {/* <input type="text" name="jobTitle" id="jobTitle" className="form-control" 
                                            value={jobTitle} onChange={(input) => setJobTitle(input.target.value)} /> */}
                                            <select value={jobTitle} className="form-control" name="jobTitle" id="jobTitle" onChange={event => jobTitleChanged(event, setJobTitle)}>
                                                <option value="">Select</option>
                                                {jobTitleDuties.map((jobTitleDuty, index) => (
                                                    <option key={index} value={jobTitleDuty.jobTitle}>{jobTitleDuty.jobTitle}</option>
                                                ))}
                                            </select>
                                            <small className="form-text text-danger">{jobTitleErr}</small>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <label htmlFor="employer" className="control-label">Employer</label>
                                            <input type="text" name="employer" id="employer" className="form-control"
                                                value={employer} onChange={(input) => setEmployer(input.target.value)} />
                                            <small className="form-text text-danger">{employerErr}</small>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <label htmlFor="city" className="control-label">City</label>
                                            <input type="text" name="city" id="city" className="form-control"
                                                value={city} onChange={(input) => setCity(input.target.value)} />
                                            <small className="form-text text-danger">{cityErr}</small>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <label htmlFor="state" className="control-label">State/Province</label>
                                            <input type="text" name="state" id="state" className="form-control"
                                                value={state} onChange={(input) => setState(input.target.value)} />
                                            <small className="form-text text-danger">{stateErr}</small>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <label htmlFor="employment-start" className="control-label">Start Date</label>
                                            <div className="input-group mb-3 input-group">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text"><i className="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                                <input type="text" name="employment-start" id="employment-start" className="form-control" value={employmentStart}
                                                    onFocus={() => openCalendar('start')} readOnly
                                                />
                                            </div>
                                            <small className="form-text text-danger">{employmentStartErr}</small>
                                        </div>
                                        <Calendar
                                            className="datepicker start"
                                            defaultView="year"
                                            view="year"
                                            prev2Label={null}
                                            next2Label={null}
                                            maxDetail="year"
                                            minDetail="decade"
                                            formatMonth={(locale, date) => date.toLocaleDateString("en-US", { month: 'short' })}
                                            onChange={(date) => setCalendar(date, "start", setEmploymentStart)}
                                        />
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <label htmlFor="employment-end" className="control-label">End Date</label>
                                            <div className="input-group mb-3 input-group">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text"><i className="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                                <input type="text" name="employment-end" id="employment-end" className="form-control" value={employmentEnd}
                                                    disabled={isPresentWork} onFocus={() => openCalendar('end')} readOnly
                                                />
                                            </div>
                                            <small className="form-text text-danger">{employmentEndErr}</small>
                                        </div>
                                        <Calendar
                                            className="datepicker end"
                                            defaultView="year"
                                            view="year"
                                            prev2Label={null}
                                            next2Label={null}
                                            maxDetail="year"
                                            minDetail="decade"
                                            formatMonth={(locale, date) => date.toLocaleDateString("en-US", { month: 'short' })}
                                            onChange={(date) => setCalendar(date, "end", setEmploymentEnd)}
                                        />
                                        <div className="form-check">
                                            <input type="checkbox" className="form-check-input" id="present-work" name="present-work" checked={isPresentWork}
                                                onChange={event => togglePresentWork(event)} />
                                            <label className="form-check-label" htmlFor="present-work">I still work here</label>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col">
                                        <div className="form-group">
                                            <label htmlFor="description" className="control-label">Description</label>
                                            <textarea type="text" name="description" id="description" className="form-control" rows="5"
                                                placeholder="Type in a brief description about the company and what you did while there."
                                                value={description}
                                                onChange={(input) => setDescription(input.target.value)}>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>

                                <div className="box box-2 mt-4">
                                    <div className="row">
                                        <div className="col">
                                            <h5>Duties</h5>

                                            <div className="input-repeater">
                                                {duties && duties.map((duty, index) => (
                                                    <div className="row" key={Math.random()}>
                                                        <div className="col">
                                                            <div className="form-group">
                                                                <p className="action-remove text-right mb-1" onClick={() => removeDuty(index)}><i className="fa fa-trash-o" aria-hidden="true"></i></p>
                                                                {/* <label htmlFor={"awards" + index} className="control-label">Organizations / Awards / Accomplishments</label> */}
                                                                <input type="text" name={"duty" + index} id={"duty" + index} className="form-control pr-5" value={duty}
                                                                    placeholder="Managed to..., Developed..., Traveled to... etc."
                                                                    onChange={(input) => updateDefaultDuties(input.target.value, index)} />
                                                            </div>
                                                        </div>
                                                    </div>
                                                ))}
                                            </div>

                                            <div className="row">
                                                <div className="col">
                                                    <div className="row form-group">
                                                        {/* <input type="text" name="classtoadd" id="classtoadd" className="form-control"
                                                        placeholder="Managed to..., Developed..., Traveled to... etc."
                                                        value={dutyToAdd}
                                                        onChange={input => setDutyToAdd(input.target.value)} /> */}
                                                        <div className='col-md-6'>
                                                            <select value={dutyToAdd}
                                                                className="form-control"
                                                                name="dutyToAdd"
                                                                id="dutyToAdd"
                                                                onChange={event => setDutyToAdd(event.target.value)}
                                                                disabled={customDuty}
                                                            >
                                                                <option value="">Select</option>
                                                                {selectedJobDuties.map((duty, index) => (
                                                                    <option key={index} value={duty}>{duty}</option>
                                                                ))}
                                                            </select>
                                                        </div>
                                                        <div className='col-1'>
                                                            <p>or</p>
                                                        </div>
                                                        <div className='col-md-5'>
                                                            <input placeholder="your custom duty here..."
                                                                type="text"
                                                                name="custom_duty"
                                                                id="custom_duty"
                                                                className="form-control"
                                                                value={customDuty}
                                                                onChange={e => setCustomDuty(e.target.value)}
                                                                onKeyPress={(event) => customDutyEntered(event)}
                                                                disabled={dutyToAdd}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col">
                                            <button className="btn btn-outline-default btn-sm" onClick={() => addDuty()}><i className="fa fa-plus" aria-hidden="true"></i> Add Another Duty</button>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div className="row mt-3">
                                <div className="col text-center">
                                    <button onClick={() => addjobExperience()} className="btn btn-outline-primary btn-sm"><i className="fa fa-plus" aria-hidden="true"></i> Add Another</button>
                                </div>
                            </div>

                            <hr className="mt-5" />

                            <div className="row">
                                <div className="col">
                                    <button className="btn btn-outline-dark px-4" onClick={() => history.push(routePrefix + "/awards")}>Back</button>
                                </div>
                                <div className="col text-right">
                                    <button className="btn btn-primary px-4" onClick={() => updateJobExperience()}>Preview</button>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </>
    );
};

export default JobExperienceSection;