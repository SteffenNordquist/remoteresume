import React, {useReducer} from 'react';
import { Route, Redirect, Switch } from "react-router-dom";
import { useHistory } from "react-router-dom";
import {resumeReducer} from '../../../reducers/resumeContextReducer';
import ContactSection from './bt1-contact';
import ObjectiveSection from './bt1-objective';
import EducationSection from './bt1-education';
import AwardsSection from './bt1-awards';
import JobExperienceSection from './bt1-jobexperience';
import Preview from './bt1-preview-full';

import BeginnerTemplate1Preview from './bt1-preview';

import '../../buildResume/buildResume.css';

import ResumeContext from '../../../providers/resume-context';


const BeginnerTemplate1 = ({loadedResumeData}) => {
    
    let history = useHistory();

    const routePrefix = '/build-resume-beginner-template-1';
    const [resumeState, dispatch] = useReducer(resumeReducer, loadedResumeData);

    const saveResumeData = (experience, template) =>{
        dispatch({type:'SAVE_RESUME', experience:experience, template:template});
    }

    const isEmptySection = (obj) => {
        return Object.entries(obj).length === 0 && obj.constructor === Object;
    }

    return (
        <ResumeContext.Provider value={{resumeData:resumeState,saveResumeData:saveResumeData}}>
            <div className="build-resume-container container-fluid">
                <div className="row">
                    <div className="col build-resume-form">
                        <div className="container">
                        
                            <div className="content">
                                
                                <div className="row">
                                    <div className="build-resume mb-5 col">
                                        <div className="center-xy">
                                            <Switch>
                                                <Route path={routePrefix + "/contact"} render={() => <ContactSection routePrefix={routePrefix} />}></Route>
                                                <Route path={routePrefix + "/objective"} render={() => <ObjectiveSection routePrefix={routePrefix} />}></Route>
                                                <Route path={routePrefix + "/education"} render={() => <EducationSection routePrefix={routePrefix} />}></Route>
                                                <Route path={routePrefix + "/awards"} render={() => <AwardsSection routePrefix={routePrefix} />}></Route>
                                                <Route path={routePrefix + "/job-experience"} render={() => <JobExperienceSection routePrefix={routePrefix} />}></Route>
                                                <Route path={routePrefix + "/preview"} render={() => <Preview routePrefix={routePrefix} />}></Route>
                                                {
                                                    (isEmptySection(loadedResumeData.contact) || !loadedResumeData.contact.firstname || !loadedResumeData.contact.lastname) &&
                                                    <Redirect from={routePrefix} to={routePrefix + "/contact"}></Redirect>
                                                }
                                                {
                                                    (isEmptySection(loadedResumeData.objective) || loadedResumeData.objective.length === 0) &&
                                                    <Redirect from={routePrefix} to={routePrefix + "/objective"}></Redirect>
                                                }
                                                {
                                                    (isEmptySection(loadedResumeData.educations) || !loadedResumeData.educations || loadedResumeData.educations.length === 0) &&
                                                    <Redirect from={routePrefix} to={routePrefix + "/education"}></Redirect>
                                                }
                                                {
                                                    (isEmptySection(loadedResumeData.awards) || !loadedResumeData.awards || loadedResumeData.awards.length === 0) &&
                                                    <Redirect from={routePrefix} to={routePrefix + "/awards"}></Redirect>
                                                }
                                                {
                                                    (isEmptySection(loadedResumeData.jobExperiences) || !loadedResumeData.jobExperiences || loadedResumeData.jobExperiences.length === 0) &&
                                                    <Redirect from={routePrefix} to={routePrefix + "/job-experience"}></Redirect>
                                                }
                                                <Redirect from={routePrefix} to={routePrefix + "/contact"}></Redirect>
                                            </Switch>
                                        </div>
                                    </div>
                                </div>
                            

                            </div>
                            
                        </div>
                    </div>
                    {          
                    history.location.pathname != "/build-resume-beginner-template-1/preview" 
                    && 
                    <div className="col resume-preview" style={{backgroundColor:'#eee'}}>
                        <BeginnerTemplate1Preview routePrefix={routePrefix} />
                    </div>
                    }
                    
                </div>
            </div>
        </ResumeContext.Provider>
    );
};

export default BeginnerTemplate1;