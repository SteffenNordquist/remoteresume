import React, { useState, useContext } from 'react';
import { useHistory } from "react-router-dom";
import ResumeContext from '../../../providers/resume-context';

const ObjectiveSection = (props) => {

    const {resumeData, saveResumeData} = useContext(ResumeContext);

    const [heading, setHeading] = useState(resumeData.objective.heading ? resumeData.objective.heading : "profile")
    const [summary, setSummary] = useState(resumeData.objective.summary ? resumeData.objective.summary : "");
    const [objective, setObjective] = useState(resumeData.objective.objective ? resumeData.objective.objective : "");
    const [profileItems, setProfileItems] = useState(resumeData.objective.profileItems ? resumeData.objective.profileItems : []); 
    const [profileItemToAdd, setProfileItemToAdd] = useState("");

    const [inputErr, setInputErr] = useState('');
    const {routePrefix} = props;

    let history = useHistory();

    const addProfileItem = () => {

        if (profileItemToAdd) {
            setProfileItems([...profileItems, profileItemToAdd]);
            setProfileItemToAdd("");
        }
    };

    const canGoNext = () => {
        if((heading === "summary" && summary) || (heading === "objective" && objective) || (heading === "profile" && profileItems.length> 0)){
            return true;
        }
        return false;
    }

    const keyPressedOnProfileItemAdding = (event) => {
        if (event.key === 'Enter') {
            addProfileItem();
        }
    }

    const removeProfileItem = (index) => {
        profileItems.splice(index, 1);

        setProfileItems([...profileItems]);
    };

    const setText = (value) => {

        if(heading === "summary")
            setSummary(value);
        if(heading === "objective")
            setObjective(value);
    };

    const updateDefaultProfileItems = (input, index) => {

        profileItems[index] = input;
        setProfileItems(profileItems);
    };

    const updateInputErrors = () =>{
        
        if(heading === "summary")
            (function(){!summary ? setInputErr("Summary is required") : setInputErr("")})();

        if(heading === "objective")
            (function(){!objective ? setInputErr("Objective is required") : setInputErr("")})();

        if(heading === "profile")
            (function(){profileItems.length == 0 ? setInputErr("Atleast one profile is required") : setInputErr("")})();
            
    }

    const updateObjective = () => {
        
        updateInputErrors();

        if(!canGoNext())
        {
            return;
        }

        if(heading === "profile") {
            resumeData.objective = {
                heading: heading,
                profileItems: profileItemToAdd ? [...profileItems, profileItemToAdd] : [...profileItems]
            };
        } else if (heading === "summary") {
            resumeData.objective = {
                heading: heading,
                summary: summary
            };
        } else if (heading === "objective") {
            resumeData.objective = {
                heading: heading,
                objective: objective
            };
        }

        saveResumeData("beginner","template1", resumeData);

        history.push(routePrefix + "/education");
    }

    return (
        <>
            <div className="row w-100">
            <div className="col">
                <div className="row">
                    <div className="col">

                        <div className="intro-text my-5">
                            <p>Select Section Heading</p>
                            <div className="btn-group btn-group-lg mb-3" role="group">
                                <button className={`btn btn-outline-dark ${heading === "profile" ? "active" : ""}`} onClick={() => {setHeading("profile");setInputErr("")}}>Profile</button>
                                <button className={`btn btn-outline-dark ${heading === "summary" ? "active" : ""}`} onClick={() => {setHeading("summary");setInputErr("")}}>Summary</button>
                                <button className={`btn btn-outline-dark ${heading === "objective" ? "active" : ""}`} onClick={() => {setHeading("objective");setInputErr("")}}>Objective</button>
                            </div>
                            <p className="lead">Action summary that describes who you are as a professional. Add qualities from the job lead.</p>
                        </div>

                        {heading == "profile" ?
                        <>
                            <div className="input-repeater">
                                <label className="control-label mb-0"><strong>Profile</strong></label>
                                <hr/>
                                {profileItems.map((item, index) => (
                                <div className="row" key={Math.random()}>
                                    <div className="col">
                                        <div className="form-group">
                                            <p className="action-remove text-right mb-1" onClick={() => removeProfileItem(index)}><i className="fa fa-trash-o" aria-hidden="true"></i></p>
                                            <input type="text" name={"profileItem" + index} id={"profileItem" + index} className="form-control pr-5" defaultValue={item}
                                                placeholder=""
                                                onChange={(input) => updateDefaultProfileItems(input.target.value, index)} />
                                        </div>
                                    </div>
                                </div>
                                ))}
                            </div>
                            <div className="row">
                                <div className="col">
                                    <div className="form-inline">
                                        {/* <label htmlFor="itemToAdd" className="control-label">Add here..</label> */}
                                        <input type="text" name="itemToAdd" id="itemToAdd" className="form-control mr-2" rows="3"
                                            placeholder="Add profile..."
                                            value={profileItemToAdd}
                                            onChange={(input) => setProfileItemToAdd(input.target.value)} 
                                            onKeyPress={(event) => keyPressedOnProfileItemAdding(event)}/>
                                        <button className="btn btn-outline-primary btn-sm" onClick={() => addProfileItem()}><i className="fa fa-plus" aria-hidden="true"></i> Add</button>
                                    </div>
                                </div>
                            </div>
                        </>
                        :
                        <div className="row">
                            <div className="col">
                                <div className="form-group">
                                    <label htmlFor="objective" className="control-label mb-0" style={{textTransform:"capitalize"}}><strong>{heading}</strong></label>
                                    <hr/>
                                    <textarea type="text" name="objective" id="objective" className="form-control" rows="5"
                                        placeholder="Action summary that describes who you are as a professional. Add qualities from the job lead."
                                        value={heading == "summary" ? summary : objective}
                                        onChange={(input) => setText(input.target.value)}>
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        }
                        <small className="form-text text-danger">{inputErr}</small>
                        <hr className="mt-5" />

                        <div className="row">
                            <div className="col">
                                <button className="btn btn-outline-dark px-4" onClick={() => history.push(routePrefix + "/contact")}>Back</button>
                            </div>
                            <div className="col text-right">
                                <button className="btn btn-primary px-4" onClick={() => updateObjective()}>Next</button>
                            </div>
                        </div>
                        

                    </div>
                </div>

            </div>
            </div>
        </>
    );
};

export default ObjectiveSection;