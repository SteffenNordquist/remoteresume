import React from 'react';

import ResumePreview from './bt1-preview'

import '../../buildResume/buildResume.css';


const ResumePreviewPage = () => {

    const routePrefix = '/build-resume';
    
    // const resumeDataDefault = {
    //     experience: '',
    //     template: '',
    //     contact: {},
    //     objective: '',
    //     educations: [],
    //     awards: [],
    //     jobExperiences: []
    // };

    //var resumeData = resumeDataDefault;
    //localStorage.clear();
    // if (!localStorage.getItem("resumeData")) {
    //     localStorage.setItem("resumeData", JSON.stringify(resumeDataDefault));
    // } else {
        //resumeData = JSON.parse(localStorage.getItem("resumeData"));
    // }


    return (
        <div className="build-resume-container container-fluid">
            <div className="row">
                <div className="col">
                    <ResumePreview routePrefix={routePrefix} />
                </div>
            </div>
        </div>
        
    );
};

export default ResumePreviewPage;