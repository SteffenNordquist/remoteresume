import React, { useState, useContext } from 'react';
import { Link, useHistory } from "react-router-dom";
import Template1, {PDFDoc, /*PDFDocRenderContainer*/} from '../../../templates/template1';
import { PDFDownloadLink, /*PDFViewer*/  } from '@react-pdf/renderer';
import ResumeContext from '../../../providers/resume-context';

const BeginnerTemplate1Preview = () => {

    let history = useHistory();
    const routePrefix = '/build-resume-beginner-template-1';
    const resumeDataContext = useContext(ResumeContext);
    const [resumeData] = useState(resumeDataContext.resumeData);
    
    return (
            <>
            {   
                history.location.pathname === "/build-resume-beginner-template-1/preview" ?
                <>
                    <div className="row">
                        <div className="col-md-3">

                            <div className="section-navigation mt-5 px-3">
                                <nav id="side-nav">
                                    <h2>Resume Sections</h2>
                                    <hr/>

                                    <ul style={{lineHeight:3,listStyleType:"none",paddingLeft:"5px"}}>
                                        <li><Link to={routePrefix + "/contact"}><i className="fa fa-address-book-o" aria-hidden="true"></i> Heading</Link></li>
                                        <li><Link to={routePrefix + "/objective"}><i className="fa fa-bars" aria-hidden="true"></i> Summary/Profile/Objective</Link></li>
                                        <li><Link to={routePrefix + "/education"}><i className="fa fa-graduation-cap" aria-hidden="true"></i> Education</Link></li>
                                        <li><Link to={routePrefix + "/awards"}><i className="fa fa-trophy" aria-hidden="true"></i> Awards/Accomplishments</Link></li>
                                        <li><Link to={routePrefix + "/job-experience"}><i className="fa fa-users" aria-hidden="true"></i> Job Experience</Link></li>
                                    </ul>
                                </nav>
                            </div>
                                
                        </div>
                        <div className="col" style={{backgroundColor:'#eee'}}>
                            <div className="preview-controls text-center mt-5 mb-3">
                                <button className="btn btn-outline-dark px-4 btn-sm" onClick={() => history.goBack()}>Back</button>
                                <PDFDownloadLink document={<PDFDoc resumeData={resumeData}/>} fileName={resumeData.contact.lastname + "_resume.pdf"}>
                                    {({ blob, url, loading, error }) => (loading ? ' Loading document...' : ' Download PDF')}
                                </PDFDownloadLink>
                            </div>
                            <Template1 
                            firstname={resumeData.contact.firstname} 
                            lastname={resumeData.contact.lastname} 
                            contactCity={resumeData.contact.city} 
                            contactState={resumeData.contact.state} 
                            contactZip={resumeData.contact.zipcode}
                            contactPhone={resumeData.contact.phone}
                            contactEmail={resumeData.contact.email}
                            linkedIn={resumeData.contact.linkedIn}
                            objective={resumeData.objective}
                            educations = {resumeData.educations}
                            awards = {resumeData.awards}
                            jobExperiences = {resumeData.jobExperiences}
                            />
                            {/* {ReactDOM.render(<PDFDocRenderContainer/>)} */}
                            {/* {ReactDOM.render(<PDFViewer ><PDFDoc /></PDFViewer>, document.getElementById('react-pdf-container'))} */}
                        </div>
                    </div>
                </>
                :
                <>
                    <div className="resume-controls text-center mt-5 mb-3">
                        <span onClick={() => history.push(routePrefix + "/preview")}>
                            <i className="fa fa-eye" aria-hidden="true"></i> Full View
                        </span>
                    </div>
                    <Template1 
                    firstname={resumeData.contact.firstname} 
                    lastname={resumeData.contact.lastname} 
                    contactCity={resumeData.contact.city} 
                    contactState={resumeData.contact.state} 
                    contactZip={resumeData.contact.zipcode}
                    contactPhone={resumeData.contact.phone}
                    contactEmail={resumeData.contact.email}
                    linkedIn={resumeData.contact.linkedIn}
                    objective={resumeData.objective}
                    educations = {resumeData.educations}
                    awards = {resumeData.awards}
                    jobExperiences = {resumeData.jobExperiences}
                    />
                </>
            }
            </>
        
    );
};

export default BeginnerTemplate1Preview;