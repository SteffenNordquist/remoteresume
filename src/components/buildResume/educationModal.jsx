import React, { useState } from 'react';

import $ from 'jquery';

import Calendar from 'react-calendar';

const EducationModal = ({education, index, update}) => {

    const [school, setSchool] = useState(education.school);
    const [degree, setDegree] = useState(education.degree);
    const [studyField, setStudyField] = useState(education.studyField);
    const [city, setCity] = useState(education.city);
    const [state, setState] = useState(education.state);
    const [graduateDate, setGraduateDate] = useState(education.graduateDate);
    const [classes, setClasses] = useState(education.classes ? education.classes : []);
    const [classToAdd, setClassToAdd] = useState("");

    const degrees = ["High School Diploma","Associate of Arts",
    "Associate of Science","Bachelor of Arts","Bachelor of Science",
    "Master of Arts","Master of Science","M.D.","Ph.D."];

    const updateDefaultEducation = () => {

        const educationEdited = {
            school: school,
            degree: degree,
            studyField: studyField,
            city: city,
            state: state,
            graduateDate: graduateDate,
            classes: classToAdd ? [...classes, classToAdd] : [...classes]
        };

        update(educationEdited, index);

        $('#education-modal-'+index).modal('hide');
    };

    const updateDefaultClasses = (input, index) => {
        classes[index] = input;
        setClasses(classes);
    };

    const addClass = () => {
        if (classToAdd) {
            setClasses([...classes, classToAdd]);
            setClassToAdd("");
        }
    };

    const removeClass = (index) => {
        classes.splice(index, 1);
        setClasses([...classes]);
    };

    const openCalendar = (calendarSelector) => {
        $('.react-calendar.'+ calendarSelector).css({"opacity":"1","visibility":"visible"});
    };
    const closeCalender = (calendarSelector) => {
        $('.react-calendar.'+ calendarSelector).css({"opacity":"0","visibility":"hidden"});
    };
    const setCalendar = (date, calendarSelector, setField) => {

        setField(date.toLocaleDateString("en-US", {
            month: "short",
            year: "numeric"
        }));

        closeCalender(calendarSelector);
    };

    return (
        <>
        <div id={"education-modal-"+index} className="modal fade education-modal" tabIndex="-1" role="dialog" aria-labelledby="education-modal" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered modal-lg">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{degree} @ {school}</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="school" className="control-label">School Name</label>
                                    <input type="text" name="school" id="school" className="form-control" 
                                        defaultValue={school} onChange={(input) => setSchool(input.target.value)} />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="degree" className="control-label">Degree</label>
                                    <select defaultValue={degree} className="form-control" name="degree" id="degree" onChange={event => setDegree(event.target.value)}>
                                        
                                        <option value="">Select</option>
                                        {degrees.map((degree, index) => (
                                            <option key={index} value={degree}>{degree}</option>
                                        ))}

                                    </select>
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="field" className="control-label">Field of Study</label>
                                    <input type="text" name="field" id="field" className="form-control" 
                                        disabled={(degree === "High School Diploma")} value={studyField} onChange={(input) => setStudyField(input.target.value)} />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="city" className="control-label">City</label>
                                    <input type="text" name="city" id="city" className="form-control"
                                        defaultValue={city} onChange={(input) => setCity(input.target.value)} />
                                </div>
                            </div>
                            <div className="col-sm-3">
                                <div className="form-group">
                                    <label htmlFor="state" className="control-label">State/Province</label>
                                    <input type="text" name="state" id="state" className="form-control"
                                        defaultValue={state} onChange={(input) => setState(input.target.value)} />
                                </div>
                            </div>
                            <div className="col-sm-3">
                                <div className="form-group">
                                    <label htmlFor="graduate-date" className="control-label">Graduation Date</label>
                                    <div className="input-group mb-3 input-group">
                                        <div className="input-group-prepend">
                                        <span className="input-group-text"><i className="fa fa-calendar" aria-hidden="true"></i></span>
                                        </div>
                                        <input type="text" name="graduate-date" id="graduate-date" className="form-control" value={graduateDate}
                                            onFocus={() => openCalendar('graduate-date')} readOnly
                                            />
                                    </div>
                                </div>
                                <Calendar 
                                    className="datepicker graduate-date"
                                    defaultView="year" 
                                    view="year" 
                                    prev2Label={null} 
                                    next2Label={null} 
                                    maxDetail="year"
                                    minDetail="decade"
                                    formatMonth={(locale, date) => date.toLocaleDateString("en-US", {month: 'short'})} 
                                    onChange={(date) => setCalendar(date, "graduate-date", setGraduateDate)}
                                    />
                            </div>
                        </div>

                        <div className="row mt-4">
                            <div className="col">
                                <h4>Classes</h4>

                                <div className="input-repeater">
                                    {classes && classes.map((schoolClass, index) => (
                                    <div className="row" key={Math.random()}>
                                        <div className="col">
                                            <div className="form-group">
                                                <p className="action-remove text-right mb-1" onClick={() => removeClass(index)}><i className="fa fa-trash-o" aria-hidden="true"></i></p>
                                                {/* <label htmlFor={"awards" + index} className="control-label">Organizations / Awards / Accomplishments</label> */}
                                                <input type="text" name={"school-class" + index} id={"school-class" + index} className="form-control pr-5" defaultValue={schoolClass}
                                                    placeholder=""
                                                    onChange={(input) => updateDefaultClasses(input.target.value, index)} />
                                            </div>
                                        </div>
                                    </div>
                                    ))}
                                </div>

                                <div className="row">
                                    <div className="col">
                                        <div className="form-group">
                                            <label htmlFor="classtoadd" className="control-label">Add here...</label>
                                            <input type="text" name="classtoadd" id="classtoadd" className="form-control"
                                                placeholder=""
                                                value={classToAdd}
                                                onChange={(input) => setClassToAdd(input.target.value)} />
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div className="row">
                            <div className="col">
                                <button className="btn btn-outline-default btn-sm" onClick={() => addClass()}><i className="fa fa-plus" aria-hidden="true"></i> Add Class</button>
                            </div>
                        </div>

                    </div>

                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" className="btn btn-primary" onClick={() => updateDefaultEducation()}>Save changes</button>
                    </div>

                </div>
            </div>
        </div>
        </>
    );
};

export default EducationModal;