import React from 'react';
import { useHistory } from "react-router-dom";

const ExperienceLevel = (props) => {

    const {routePrefix, ...rest} = props;

    let history = useHistory();

    const updateExperience = (experience) => {

        // var resumeData = JSON.parse(localStorage.getItem("resumeData"));
        // resumeData.experience = experience;

        // localStorage.setItem("resumeData", JSON.stringify(resumeData));

        history.push(routePrefix + `/${experience}-templates`);
    }

    return (
        
        <div className="row w-100">
            <div className="col text-center">

                <div className="row">
                    <div className="col">
                        <div className="intro-text my-5">
                            <h1>We'd like to know your level of experience!</h1>
                            <p className="lead">Some additional text about this part</p>
                        </div>
                    </div>
                </div>
                
                <div className="row options">
                    <div className="col-sm-4">
                        <div className="box shadow" onClick={() => updateExperience('beginner')}>
                            <h3>Beginner</h3>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="box shadow" onClick={() => /*updateExperience('intermediate')*/ alert("templates under construction")}>
                            <h3>Intermediate</h3>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="box shadow" onClick={() => /*updateExperience('expert')*/ alert("templates under construction")}>
                            <h3>Expert</h3>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        
    );
};

export default ExperienceLevel;