import React, { useState } from 'react';

import $ from 'jquery';

import Calendar from 'react-calendar';

const JobExperienceModal = ({jobExperience, index, update}) => {

    const [employmentStart, setEmploymentStart] = useState(jobExperience.employmentStart ? jobExperience.employmentStart : "");
    const [employmentEnd, setEmploymentEnd] = useState(jobExperience.employmentEnd ? jobExperience.employmentEnd : "");
    const [isPresentWork, setIsPresentWork] = useState(jobExperience.employmentEnd === "Present" ? true : false);

    const [jobTitle, setJobTitle] = useState(jobExperience.jobTitle ? jobExperience.jobTitle : "");
    const [employer, setEmployer] = useState(jobExperience.employer);
    const [city, setCity] = useState(jobExperience.city);
    const [state, setState] = useState(jobExperience.state);
    const [description, setDescription] = useState(jobExperience.description);

    const [duties, setDuties] = useState(jobExperience.duties ? jobExperience.duties : []);
    const [dutyToAdd, setDutyToAdd] = useState("");

    const updateDefaultjobExperience = () => {

        const jobExperienceEdited = {
            jobTitle: jobTitle,
            employer: employer,
            city: city,
            state: state,
            employmentStart: employmentStart,
            employmentEnd: employmentEnd,
            description: description,
            duties: duties
        };

        console.log(jobExperienceEdited);
        update(jobExperienceEdited, index);

        $('#job-experience-modal-'+index).modal('hide');
    };

    const updateDefaultDuties = (input, index) => {
        duties[index] = input;
        setDuties(duties);
    };

    const addDuty = () => {
        if (dutyToAdd) {
            setDuties([...duties, dutyToAdd]);
            setDutyToAdd("");
        }
    };

    const removeDuty = (index) => {
        duties.splice(index, 1);
        setDuties([...duties]);
    };

    const openCalendar = (calendarSelector) => {
        $('.react-calendar.'+ calendarSelector).css({"opacity":"1","visibility":"visible"});
    };
    const closeCalender = (calendarSelector) => {
        $('.react-calendar.'+ calendarSelector).css({"opacity":"0","visibility":"hidden"});
    };
    const setCalendar = (date, calendarSelector, setField) => {

        setField(date.toLocaleDateString("en-US", {
            month: "short",
            year: "numeric"
        }));

        closeCalender(calendarSelector);
    };

    const togglePresentWork = (event) => {

        const checked = event.target.checked;
        if (checked) {
            setEmploymentEnd("Present");
            setIsPresentWork(checked);
        } else {
            setEmploymentEnd("");
            setIsPresentWork(checked);
        }
    };

    return (
        <>
        <div id={"job-experience-modal-"+index} className="modal fade job-experience-modal" tabIndex="-1" role="dialog" aria-labelledby="job-experience-modal" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered modal-lg">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{jobTitle}</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="jobTitle" className="control-label">Job Title</label>
                                    <input type="text" name="jobTitle" id="jobTitle" className="form-control" 
                                        defaultValue={jobTitle} onChange={(input) => setJobTitle(input.target.value)} />
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="employer" className="control-label">Employer</label>
                                    <input type="text" name="employer" id="employer" className="form-control" 
                                        defaultValue={employer} onChange={(input) => setEmployer(input.target.value)} />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="city" className="control-label">City</label>
                                    <input type="text" name="city" id="city" className="form-control"
                                        defaultValue={city} onChange={(input) => setCity(input.target.value)} />
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="state" className="control-label">State/Province</label>
                                    <input type="text" name="state" id="state" className="form-control"
                                        defaultValue={state} onChange={(input) => setState(input.target.value)} />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor={"employment-start"+index} className="control-label">Start Date</label>
                                    <div className="input-group mb-3 input-group">
                                        <div className="input-group-prepend">
                                        <span className="input-group-text"><i className="fa fa-calendar" aria-hidden="true"></i></span>
                                        </div>
                                        <input type="text" name={"employment-start"+index} id={"employment-start"+index} className="form-control" value={employmentStart}
                                            onFocus={() => openCalendar('start-'+index)} readOnly
                                            />
                                    </div>
                                </div>
                                <Calendar 
                                    className={"datepicker start-"+index}
                                    defaultView="year" 
                                    view="year" 
                                    prev2Label={null} 
                                    next2Label={null} 
                                    maxDetail="year"
                                    minDetail="decade"
                                    formatMonth={(locale, date) => date.toLocaleDateString("en-US", {month: 'short'})} 
                                    onChange={(date) => setCalendar(date, "start-"+index, setEmploymentStart)}
                                    />
                            </div>
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor={"employment-end"+index} className="control-label">End Date</label>
                                    <div className="input-group mb-3 input-group">
                                        <div className="input-group-prepend">
                                        <span className="input-group-text"><i className="fa fa-calendar" aria-hidden="true"></i></span>
                                        </div>
                                        <input type="text" name={"employment-end"+index} id={"employment-end"+index} className="form-control" value={employmentEnd}
                                            disabled={isPresentWork} onFocus={() => openCalendar('end-'+index)} readOnly
                                            />
                                    </div>
                                </div>
                                <Calendar 
                                    className={"datepicker end-"+index}
                                    defaultView="year" 
                                    view="year" 
                                    prev2Label={null} 
                                    next2Label={null} 
                                    maxDetail="year"
                                    minDetail="decade"
                                    formatMonth={(locale, date) => date.toLocaleDateString("en-US", {month: 'short'})} 
                                    onChange={(date) => setCalendar(date, "end-"+index, setEmploymentEnd)}
                                    />
                                <div className="form-check">
                                    <input type="checkbox" className="form-check-input" id={"present-work-"+index} name={"present-work-"+index} defaultChecked={isPresentWork}
                                        onChange={event => togglePresentWork(event)} />
                                    <label className="form-check-label" htmlFor={"present-work-"+index}>I still work here</label>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col"> 
                                <div className="form-group">
                                    <label htmlFor={"description-"+index} className="control-label">Description</label>
                                    <textarea type="text" name={"description-"+index} id={"description-"+index} className="form-control" rows="5"
                                        placeholder="Type in a brief description about the company and what you did while there."
                                        defaultValue={description}
                                        onChange={(input) => setDescription(input.target.value)}>
                                    </textarea>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col">
                                <h5>Duties</h5>

                                <div className="input-repeater">
                                    {duties && duties.map((duty, index) => (
                                    <div className="row" key={Math.random()}>
                                        <div className="col">
                                            <div className="form-group">
                                                <p className="action-remove text-right mb-1" onClick={() => removeDuty(index)}><i className="fa fa-trash-o" aria-hidden="true"></i></p>
                                                {/* <label htmlFor={"awards" + index} className="control-label">Organizations / Awards / Accomplishments</label> */}
                                                <input type="text" name={"duty" + index} id={"duty" + index} className="form-control pr-5" value={duty}
                                                    placeholder="Managed to..., Developed..., Traveled to... etc."
                                                    onChange={(input) => updateDefaultDuties(input.target.value, index)} />
                                            </div>
                                        </div>
                                    </div>
                                    ))}
                                </div>

                                <div className="row">
                                    <div className="col">
                                        <div className="form-group">
                                            <label htmlFor="classtoadd" className="control-label">Add here...</label>
                                            <input type="text" name="classtoadd" id="classtoadd" className="form-control"
                                                placeholder=""
                                                value={dutyToAdd}
                                                onChange={input => setDutyToAdd(input.target.value)} />
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div className="row">
                            <div className="col">
                                <button className="btn btn-outline-default btn-sm" onClick={() => addDuty()}><i className="fa fa-plus" aria-hidden="true"></i> Add Duty</button>
                            </div>
                        </div>

                    </div>

                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" className="btn btn-primary" onClick={() => updateDefaultjobExperience()}>Save changes</button>
                    </div>

                </div>
            </div>
        </div>
        </>
    );
};

export default JobExperienceModal;