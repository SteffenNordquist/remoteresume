import React, { useState, useContext } from 'react';
import { useHistory } from "react-router-dom";
import CoverLetterContext from '../../../providers/cover-letter-context';

const SignatureSection = (props) => {

    const {coverLetterData, saveCoverLetterData} = useContext(CoverLetterContext);
    const [closing, setClosing] = useState(coverLetterData.closingParagraph);
    const [closingErr, setClosingErr] = useState('');
    const [thankyou, setThankyou] = useState(coverLetterData.thankyouParagraph);
    const [thankyouErr, setThankyouErr] = useState('');
    const {routePrefix} = props;

    let history = useHistory();

    const updateContact = () => {
        
        updateClosingErrors();
        updateThankyouErrors();

        if(!canGoNext())
        {
            return;
        }
        
        coverLetterData.closingParagraph = closing;
        coverLetterData.thankyouParagraph = thankyou;
        
        saveCoverLetterData("coverletter1", coverLetterData);

        history.push(routePrefix + "/signature");
    }

    const canGoNext = () => {
        if(closing && thankyou){
            return true;
        }
        return false; 
    }

    const updateClosingErrors = () =>{
        
        (function(){!closing ? setClosingErr("Your input is required") : setClosingErr("")})();
            
    }

    const updateThankyouErrors = () =>{
        
        (function(){!thankyou ? setThankyouErr("Your input is required") : setThankyouErr("")})();
            
    }

    return (
        <>
            <div className="row w-100">
            <div className="col">

                <div className="row">
                    <div className="col">
                        <div className="intro-text my-5">
                            <h2>Closing Paragraph</h2>
                            <p>Your closing. Summarize what you would bring to the position and suggest next steps
                                        by requesting a meeting or suggesting a call.</p>
                        </div>

                        <div className="row">
                            <div className="col">
                                <div className="form-group">
                                    <textarea type="text" name="closing" id="closing" className="form-control" rows="5"
                                        placeholder="Your closing paragraph"
                                        value={closing}
                                        onChange={(closing) => setClosing(closing.target.value)}>
                                    </textarea>
                                    <small className="form-text text-danger">{closingErr}</small>
                                </div>
                            </div>
                        </div>                     

                    </div>
                </div>

                <div className="row">
                    <div className="col">
                        <div className="intro-text my-5">
                            <h2>Thank You Paragraph</h2>
                            <p>Thank the HR manager for their time.</p>
                        </div>

                        <div className="row">
                            <div className="col">
                                <div className="form-group">
                                    <textarea type="text" name="thankyou" id="thankyou" className="form-control" rows="5"
                                        placeholder="Your thank you paragraph"
                                        value={thankyou}
                                        onChange={(thankyou) => setThankyou(thankyou.target.value)}>
                                    </textarea>
                                    <small className="form-text text-danger">{thankyouErr}</small>
                                </div>
                            </div>
                        </div>

                        <hr className="mt-5" />

                        <div className="row">
                            <div className="col">
                                <button className="btn btn-outline-dark px-4" onClick={() => history.push("../create-cover-letter-cover-letter-1/selling-yourself")}>Back</button>
                            </div>
                            <div className="col text-right">
                                <button className="btn btn-primary px-4" onClick={() => updateContact()}>Next</button>
                            </div>
                        </div>
                        

                    </div>
                </div>

            </div>
            </div>
        </>
    );
};

export default SignatureSection;