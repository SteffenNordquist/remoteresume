import React, { useState, useContext } from 'react';
import { useHistory } from "react-router-dom";
import CoverLetterContext from '../../../providers/cover-letter-context';
import $ from 'jquery';
import Calendar from 'react-calendar';

const HeadingSection = (props) => {
    const {coverLetterData, saveCoverLetterData} = useContext(CoverLetterContext);
    const [date, setDate] = useState(coverLetterData.heading.date);
    const [dateErr, setDateErr] = useState('');
    const [managerName, setManagerName] = useState(coverLetterData.heading.managerName);
    const [managerNameErr, setManagerNameErr] = useState('');
    const [companyName, setCompanyName] = useState(coverLetterData.heading.companyName);
    const [companyNameErr, setCompanyNameErr] = useState('');
    const [companyAddress1, setCompanyAddress1] = useState(coverLetterData.heading.companyAddress1);
    const [companyAddress1Err, setCompanyAddress1Err] = useState('');
    const [companyAddress2, setCompanyAddress2] = useState(coverLetterData.heading.companyAddress2);
    const {routePrefix} = props;

    let history = useHistory();

    const updateContact = () => {
        
        updateDateError();
        updateManagerNameError();
        updateCompanyNameError();
        updateCompanyAddress1Error();

        if(!canGoNext())
        {
            return;
        }
        
        coverLetterData.heading.date = date;
        coverLetterData.heading.managerName = managerName;
        coverLetterData.heading.companyName = companyName;
        coverLetterData.heading.companyAddress1 = companyAddress1;
        coverLetterData.heading.companyAddress2 = companyAddress2;
        
        saveCoverLetterData("coverletter1", coverLetterData);

        history.push(routePrefix + "/purpose-of-letter");
    }

    const canGoNext = () => {
        if(date && managerName && companyName && companyAddress1){
            return true;
        }
        return false; 
    }
    const updateDateError = () =>{      
        (function(){!date ? setDateErr("Manager name is required") : setDateErr("")})();            
    }
    const updateManagerNameError = () =>{      
        (function(){!managerName ? setManagerNameErr("Manager name is required") : setManagerNameErr("")})();            
    }
    const updateCompanyNameError = () =>{
        
        (function(){!companyName ? setCompanyNameErr("Company name is required") : setCompanyNameErr("")})();          
    }
    const updateCompanyAddress1Error = () =>{       
        (function(){!companyAddress1 ? setCompanyAddress1Err("Company Address 1 is required") : setCompanyAddress1Err("")})();          
    }
    const closeCalender = (calendarSelector) => {
        $('.react-calendar.'+ calendarSelector).css({"opacity":"0","visibility":"hidden"});
    };
    const openCalendar = (calendarSelector) => {
        $('.react-calendar.'+ calendarSelector).css({"opacity":"1","visibility":"visible"});
    };
    const setCalendar = (date, calendarSelector, setField) => {

        setField(date.toLocaleDateString("en-US", {
            day:"numeric",
            month: "long",
            year: "numeric"
        }));

        closeCalender(calendarSelector);
    };

    return (
        <>
            <div className="row w-100">
            <div className="col">

                <div className="row">
                    <div className="col">
                        <div className="intro-text my-5">
                            <h2>Heading</h2>
                        </div>
                        <div className="row">
                            <div className="col-sm-3">
                                <div className="form-group">
                                    <label htmlFor="date" className="control-label">Date</label>
                                    <div className="input-group mb-3 input-group">
                                        <div className="input-group-prepend">
                                        <span className="input-group-text"><i className="fa fa-calendar" aria-hidden="true"></i></span>
                                        </div>
                                        <input type="text" name="date" id="date" className="form-control" value={date}
                                            onFocus={() => openCalendar('date')} readOnly
                                            />
                                        <small className="form-text text-danger">{dateErr}</small>
                                    </div>
                                </div>
                                    <Calendar 
                                        className="datepicker date"
                                        prev2Label={null} 
                                        next2Label={null} 
                                        //maxDetail="year"
                                        minDetail="decade"
                                        formatMonth={(locale, date) => date.toLocaleDateString("en-US", {month: 'long'})} 
                                        onChange={(date) => setCalendar(date, "date", setDate)}
                                    />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="managerName" className="control-label">Manager Name</label>
                                    <input type="text" name="managerName" id="managerName" className="form-control" 
                                        defaultValue={managerName} onChange={(input) => setManagerName(input.target.value)} />
                                    <small className="form-text text-danger">{managerNameErr}</small>
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="form-group">
                                    <label htmlFor="companyName" className="control-label">Company Name</label>
                                    <input type="text" name="companyName" id="companyName" className="form-control" 
                                        defaultValue={companyName} onChange={(input) => setCompanyName(input.target.value)} />
                                    <small className="form-text text-danger">{companyNameErr}</small>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-sm-4">
                                <div className="form-group">
                                    <label htmlFor="companyAddress1" className="control-label">Company Address 1</label>
                                    <input type="text" name="companyAddress1" id="companyAddress1" className="form-control"
                                        defaultValue={companyAddress1} onChange={(input) => setCompanyAddress1(input.target.value)} />
                                    <small className="form-text text-danger">{companyAddress1Err}</small>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="form-group">
                                    <label htmlFor="companyAddress2" className="control-label">Company Address 2</label>
                                    <input type="text" name="companyAddress2" id="companyAddress2" className="form-control"
                                        defaultValue={companyAddress2} onChange={(input) => setCompanyAddress2(input.target.value)} />
                                </div>
                            </div>
                        </div>

                        <hr className="mt-5" />

                        <div className="row">
                            <div className="col">
                                <button className="btn btn-outline-dark px-4" onClick={() => history.push(`../create-cover-letter/cover-letter-templates`)}>Back</button>
                            </div>
                            <div className="col text-right">
                                <button className="btn btn-primary px-4" onClick={() => updateContact()}>Next</button>
                            </div>
                        </div>
                        

                    </div>
                </div>

            </div>
            </div>
        </>
    );
};

export default HeadingSection;