import React, {useEffect, useState} from 'react';
import {getCoverLetterData, defaultCoverLetter1} from '../../../services/persistence';
import Ct1Main from './cl1-main';
import {finalizedCoverLetter1FromSource} from '../../../utils/resumeFinalizerFromSource';
import Loader from 'react-loader';

const Ct1Init = () => {

    useEffect(() => {
        initLoad();
    }, []);
    
    const initLoad = async() =>{

        let retries = 0;
        while(true){
            try{
                const loadedCoverLetter = await getCoverLetterData("coverletter1");
                const finalLoadedCoverLetter = finalizedCoverLetter1FromSource(loadedCoverLetter);
                setLetterDoc(finalLoadedCoverLetter);
                setLoaded(true);
                break;
            }catch(err){
                retries++
                if(retries > 3){
                    alert("Error occured, please try again later.");
                    break;
                }
            }
        }
    }
    
    const [loaded, setLoaded] = useState(false);
    const [letterDoc, setLetterDoc] = useState(defaultCoverLetter1());

    return(
        <Loader loaded={loaded} scale={1.0}>
            <Ct1Main letterDoc={letterDoc}/>
        </Loader>
    );

};

export default Ct1Init;