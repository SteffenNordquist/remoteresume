import React, { useState, useContext } from 'react';
import { useHistory } from "react-router-dom";
import CoverLetterContext from '../../../providers/cover-letter-context';

const LetterPurposeSection = (props) => {

    const {coverLetterData, saveCoverLetterData} = useContext(CoverLetterContext);  
    const [input, setInput] = useState(coverLetterData.letterPurposeParagraph);
    const [inputErr, setInputErr] = useState('');
    const {routePrefix} = props;

    let history = useHistory();

    const updateContact = () => {
        
        updateInputErrors();

        if(!canGoNext())
        {
            return;
        }
        
        coverLetterData.letterPurposeParagraph = input;
        
        saveCoverLetterData("coverletter1", coverLetterData);

        history.push(routePrefix + "/more-about-yourself");
    }

    const canGoNext = () => {
        if(input){
            return true;
        }
        return false;
    }

    const updateInputErrors = () =>{
        
        (function(){!input ? setInputErr("Your input is required") : setInputErr("")})();
            
    }

    return (
        <>
            <div className="row w-100">
            <div className="col">

                <div className="row">
                    <div className="col">
                        <div className="intro-text my-5">
                            <h2>Purpose of the Letter</h2>
                            <p>Why you are writing. Use an ‘attention grabber’ to get the attention of the HR manager.
                                        Show your enthusiasm. You can do this by showing your knowledge of the company
                                        and that you did you your research and pair with how you can contribute to its mission.
                                        State the job title, and offer specific info connecting a few core strengths to the duties of
                                        the position to show that you’re the right fit.</p>
                        </div>

                        <div className="row">
                            <div className="col">
                                <div className="form-group">
                                    <textarea type="text" name="letter-purpose" id="letter-purpose" className="form-control" rows="5"
                                        placeholder="Enter purpose of your letter"
                                        value={input}
                                        onChange={(input) => setInput(input.target.value)}>
                                    </textarea>
                                    <small className="form-text text-danger">{inputErr}</small>
                                </div>
                            </div>
                        </div>

                        <hr className="mt-5" />

                        <div className="row">
                            <div className="col">
                                <button className="btn btn-outline-dark px-4" onClick={() => history.push("../create-cover-letter-cover-letter-1/heading")}>Back</button>
                            </div>
                            <div className="col text-right">
                                <button className="btn btn-primary px-4" onClick={() => updateContact()}>Next</button>
                            </div>
                        </div>
                        

                    </div>
                </div>

            </div>
            </div>
        </>
    );
};

export default LetterPurposeSection;