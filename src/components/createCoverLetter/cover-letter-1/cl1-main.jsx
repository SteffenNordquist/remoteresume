import React, {useReducer} from 'react';
import { Route, Redirect, Switch } from "react-router-dom";
import { useHistory } from "react-router-dom";
import {coverLetterReducer} from '../../../reducers/coverLetterContextReducer';
import HeadingSection from './cl1-heading';
import LetterPurposeSection from './cl1-letterpurpose';
import MoreAboutYourselfSection from './cl1-moreaboutyourself';
import SellingYourselfSection from './cl1-sellingyourself';
import ClosingThankyouSection from './cl1-closingthankyou';
import SignatureSection from './cl1-signature';
import Preview from './ct1-preview-full';

import CoverLetter1Preview from './ct1-preview';

import '../../buildResume/buildResume.css';

import CoverLetterContext from '../../../providers/cover-letter-context';


const CoverLetter1 = ({letterDoc}) => {
    
    let history = useHistory();

    const routePrefix = '/create-cover-letter-cover-letter-1';
    const [state, dispatch] = useReducer(coverLetterReducer, letterDoc);

    const saveCoverLetterData = (template) =>{
        dispatch({type:'SAVE_COVER_LETTER', template:template});
    }

    const isEmptySection = (obj) => {
        if(!obj)
        {
            return true;
        }
        return  Object.entries(obj).length === 0 && obj.constructor === Object;
    }

    return (
        <CoverLetterContext.Provider value={{coverLetterData:state,saveCoverLetterData:saveCoverLetterData}}>
            <div className="build-resume-container container-fluid">
                <div className="row">
                    <div className="col build-resume-form">
                        <div className="container">
                        
                            <div className="content">
                                
                                <div className="row">
                                    <div className="build-resume mb-5 col">
                                        <div className="center-xy">
                                            <Switch>
                                                <Route path={routePrefix + "/heading"} render={() => <HeadingSection routePrefix={routePrefix} />}></Route>
                                                <Route path={routePrefix + "/purpose-of-letter"} render={() => <LetterPurposeSection routePrefix={routePrefix} />}></Route>
                                                <Route path={routePrefix + "/more-about-yourself"} render={() => <MoreAboutYourselfSection routePrefix={routePrefix} />}></Route>
                                                <Route path={routePrefix + "/selling-yourself"} render={() => <SellingYourselfSection routePrefix={routePrefix} />}></Route>
                                                <Route path={routePrefix + "/closing-thankyou"} render={() => <ClosingThankyouSection routePrefix={routePrefix} />}></Route>
                                                <Route path={routePrefix + "/signature"} render={() => <SignatureSection routePrefix={routePrefix} />}></Route>
                                                <Route path={routePrefix + "/preview"} render={() => <Preview routePrefix={routePrefix} />}></Route>
                                                {
                                                    (isEmptySection(letterDoc.heading) || !letterDoc.heading.managerName || !letterDoc.heading.companyName || !letterDoc.heading.companyAddress1) &&
                                                    <Redirect from={routePrefix} to={routePrefix + "/heading"}></Redirect>
                                                }
                                                {
                                                    (isEmptySection(letterDoc.letterPurposeParagraph) || letterDoc.letterPurposeParagraph.length === 0) &&
                                                    <Redirect from={routePrefix} to={routePrefix + "/purpose-of-letter"}></Redirect>
                                                }
                                                {
                                                    (isEmptySection(letterDoc.moreAboutYourselfParagraph) || letterDoc.moreAboutYourselfParagraph.paragraph.length === 0) &&
                                                    <Redirect from={routePrefix} to={routePrefix + "/more-about-yourself"}></Redirect>
                                                }
                                                {
                                                    (isEmptySection(letterDoc.sellingYourselfParagraph) || letterDoc.sellingYourselfParagraph.length === 0) &&
                                                    <Redirect from={routePrefix} to={routePrefix + "/selling-yourself"}></Redirect>
                                                }
                                                {
                                                    (isEmptySection(letterDoc.closingParagraph) || !letterDoc.closingParagraph || !letterDoc.thankyouParagraph) &&
                                                    <Redirect from={routePrefix} to={routePrefix + "/closing-thankyou"}></Redirect>
                                                }
                                                {
                                                    (isEmptySection(letterDoc.signature) || letterDoc.signture.length === 0) &&
                                                    <Redirect from={routePrefix} to={routePrefix + "/signature"}></Redirect>
                                                }
                                                <Redirect from={routePrefix} to={routePrefix + "/heading"}></Redirect>                                               
                                            </Switch>
                                        </div>
                                    </div>
                                </div>
                            

                            </div>
                            
                        </div>
                    </div>
                    {          
                    history.location.pathname != "/create-cover-letter-cover-letter-1/preview" 
                    && 
                    <div className="col resume-preview" style={{backgroundColor:'#eee'}}>
                        <CoverLetter1Preview routePrefix={routePrefix} />
                    </div>
                    }
                    
                </div>
            </div>
        </CoverLetterContext.Provider>
    );
};

export default CoverLetter1;