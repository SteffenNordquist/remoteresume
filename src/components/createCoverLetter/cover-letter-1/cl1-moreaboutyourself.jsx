import React, { useState, useContext } from 'react';
import { useHistory } from "react-router-dom";
import CoverLetterContext from '../../../providers/cover-letter-context';

const MoreAboutYourselfSection = (props) => {

    const {coverLetterData, saveCoverLetterData} = useContext(CoverLetterContext);
    const [input, setInput] = useState(coverLetterData.moreAboutYourselfParagraph.paragraph);
    const [inputErr, setInputErr] = useState('');
    const [attributes, setAttributes] = useState(coverLetterData.moreAboutYourselfParagraph.attributes? coverLetterData.moreAboutYourselfParagraph.attributes:[]);
    const [attributeToAdd, setAttributeToAdd] = useState('');
    const {routePrefix} = props;

    let history = useHistory();

    const updateContact = () => {
        
        updateInputErrors();

        if(!canGoNext())
        {
            return;
        }
        
        coverLetterData.moreAboutYourselfParagraph.paragraph = input;
        coverLetterData.moreAboutYourselfParagraph.attributes = attributes;
        
        saveCoverLetterData("coverletter1", coverLetterData);

        history.push(routePrefix + "/selling-yourself");
    }

    const canGoNext = () => {
        if(input){
            return true;
        }
        return false;
    }

    const keyPressedOnAttributeAdding = (event) => {
        if (event.key === 'Enter') {
            addAttribute();
        }
    }

    const updateInputErrors = () =>{
        
        (function(){!input ? setInputErr("Your input is required") : setInputErr("")})();
            
    }

    const addAttribute = () => {
        if (attributeToAdd) {
            setAttributes([...attributes, attributeToAdd]);
            setAttributeToAdd("");
        }
    };

    const removeAttribute = (index) => {
        attributes.splice(index, 1);
        setAttributes([...attributes]);
    };

    const updateDefaultAttributes = (input, index) => {
        attributes[index] = input;
        setAttributes(attributes);
    };

    return (
        <>
            <div className="row w-100">
            <div className="col">

                <div className="row">
                    <div className="col">
                        <div className="intro-text my-5">
                            <h2>More About Yourself</h2>
                            <p>Go more in depth and tell more about what you have to offer the employer specifically
                                        related to the job description. Make sure you use phrases/language from the job
                                        description and connect your abilities once again. Bullet points in this paragraph are
                                        highly effective in drawing your reader's eye to your skills and/or successes.</p>
                        </div>

                        <div className="row">
                            <div className="col">
                                <div className="form-group">
                                    <textarea type="text" name="more-about-yourself" id="more-about-yourself" className="form-control" rows="5"
                                        placeholder="More about yourself"
                                        value={input}
                                        onChange={(input) => setInput(input.target.value)}>
                                    </textarea>
                                    <small className="form-text text-danger">{inputErr}</small>
                                </div>
                            </div>
                        </div>

                        <div className="box mt-1">
                                <div className="row">
                                    <div className="col">
                                        <h5>Attributes</h5>

                                        <div className="input-repeater">
                                            {attributes && attributes.map((attribute, index) => (
                                            <div className="row" key={Math.random()}>
                                                <div className="col">
                                                    <div className="form-group">
                                                        <p className="action-remove text-right mb-1" onClick={() => removeAttribute(index)}><i className="fa fa-trash-o" aria-hidden="true"></i></p>
                                                        <input type="text" name={"attribute" + index} id={"attribute" + index} className="form-control pr-5" value={attribute}
                                                            placeholder=""
                                                            onChange={(input) => updateDefaultAttributes(input.target.value, index)} />
                                                    </div>
                                                </div>
                                            </div>
                                            ))}
                                        </div>

                                        <div className="row">
                                            <div className="col">
                                                <div className="form-group">
                                                    <input type="text" name="attributetoadd" id="attributetoadd" className="form-control"
                                                        placeholder="Add attribute"
                                                        value={attributeToAdd}
                                                        onChange={(input) => setAttributeToAdd(input.target.value)} 
                                                        onKeyPress={(event) => keyPressedOnAttributeAdding(event)}/>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col">
                                        <button className="btn btn-outline-default btn-sm" onClick={() => addAttribute()}><i className="fa fa-plus" aria-hidden="true"></i> Add Another Attribute</button>
                                    </div>
                                </div>
                            </div>

                        <hr className="mt-5" />

                        <div className="row">
                            <div className="col">
                                <button className="btn btn-outline-dark px-4" onClick={() => history.push("../create-cover-letter-cover-letter-1/purpose-of-letter")}>Back</button>
                            </div>
                            <div className="col text-right">
                                <button className="btn btn-primary px-4" onClick={() => updateContact()}>Next</button>
                            </div>
                        </div>
                        

                    </div>
                </div>

            </div>
            </div>
        </>
    );
};

export default MoreAboutYourselfSection;