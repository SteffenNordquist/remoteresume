import React, { useState, useContext } from 'react';
import { useHistory } from "react-router-dom";
import CoverLetterContext from '../../../providers/cover-letter-context';

const SellingYourselfSection = (props) => {

    const {coverLetterData, saveCoverLetterData} = useContext(CoverLetterContext);
    const [input, setInput] = useState(coverLetterData.sellingYourselfParagraph);
    const [inputErr, setInputErr] = useState('');
    const {routePrefix} = props;

    let history = useHistory();

    const updateContact = () => {
        
        updateInputErrors();

        if(!canGoNext())
        {
            return;
        }
        
        coverLetterData.sellingYourselfParagraph = input;
        
        saveCoverLetterData("coverletter1", coverLetterData);

        history.push(routePrefix + "/closing-thankyou");
    }

    const canGoNext = () => {
        if(input){
            return true;
        }
        return false;
    }

    const updateInputErrors = () =>{
        
        (function(){!input ? setInputErr("Your input is required") : setInputErr("")})();
            
    }

    return (
        <>
            <div className="row w-100">
            <div className="col">

                <div className="row">
                    <div className="col">
                        <div className="intro-text my-5">
                            <h2>Selling Yourself</h2>
                            <p>If desired, you can add another paragraph that yet again sells yourself for the
                                        opportunity. Perhaps what you’d like to do for the company in the future, connect an
                                        experience to this opportunity’s needs, etc.</p>
                        </div>

                        <div className="row">
                            <div className="col">
                                <div className="form-group">
                                    <textarea type="text" name="selling-yourself" id="selling-yourself" className="form-control" rows="5"
                                        placeholder="Paragraph that sells yourself"
                                        value={input}
                                        onChange={(input) => setInput(input.target.value)}>
                                    </textarea>
                                    <small className="form-text text-danger">{inputErr}</small>
                                </div>
                            </div>
                        </div>

                        <hr className="mt-5" />

                        <div className="row">
                            <div className="col">
                                <button className="btn btn-outline-dark px-4" onClick={() => history.push("../create-cover-letter-cover-letter-1/more-about-yourself")}>Back</button>
                            </div>
                            <div className="col text-right">
                                <button className="btn btn-primary px-4" onClick={() => updateContact()}>Next</button>
                            </div>
                        </div>
                        

                    </div>
                </div>

            </div>
            </div>
        </>
    );
};

export default SellingYourselfSection;