import React, { useState, useContext } from 'react';
import { useHistory } from "react-router-dom";
import CoverLetterContext from '../../../providers/cover-letter-context';

const SignatureSection = (props) => {

    const {coverLetterData, saveCoverLetterData} = useContext(CoverLetterContext);
    const [yourName, setYourName] = useState(coverLetterData.yourName);
    const [yourNameErr, setYourNameErr] = useState('');
    const [yourEmail, setYourEmail] = useState(coverLetterData.yourEmail);
    const [yourEmailErr, setYourEmailErr] = useState('');
    const [yourPhone, setYourPhone] = useState(coverLetterData.yourPhone);
    const [yourPhoneErr, setYourPhoneErr] = useState('');
    const {routePrefix} = props;

    let history = useHistory();

    const updateContact = () => {
        
        updateYourNameErrors();
        updateYourEmailErrors();
        updateYourPhoneErrors();

        if(!canGoNext())
        {
            return;
        }
        
        coverLetterData.yourName = yourName;
        coverLetterData.yourEmail = yourEmail;
        coverLetterData.yourPhone = yourPhone;
        
        saveCoverLetterData("coverletter1", coverLetterData);

        history.push(routePrefix + "/preview");
    }

    const canGoNext = () => {
        if(yourName && yourEmail && yourPhone){
            return true;
        }
        return false; 
    }

    const updateYourNameErrors = () =>{
        
        (function(){!yourName ? setYourNameErr("Your name is required") : setYourNameErr("")})();
            
    }

    const updateYourEmailErrors = () =>{
        
        (function(){!yourEmail ? setYourEmailErr("Your email is required") : setYourEmailErr("")})();
            
    }

    const updateYourPhoneErrors = () =>{
        
        (function(){!yourPhone ? setYourPhoneErr("Your phone is required") : setYourPhoneErr("")})();
            
    }

    return (
        <>
            <div className="row w-100">
            <div className="col">

                <div className="row">
                    <div className="col">
                        <div className="intro-text my-5">
                            <h2>Signature</h2>
                        </div>

                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="yourName" className="control-label">Your Name</label>
                                        <input type="text" name="yourName" id="yourName" className="form-control" 
                                            defaultValue={yourName} onChange={(input) => setYourName(input.target.value)} />
                                        <small className="form-text text-danger">{yourNameErr}</small>
                                    </div>
                                </div>                           
                            </div>

                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="youEmail" className="control-label">Your Email</label>
                                        <input type="email" name="yourEmail" id="yourEmail" className="form-control"
                                            defaultValue={yourEmail} onChange={(input) => setYourEmail(input.target.value)} />
                                        <small className="form-text text-danger">{yourEmailErr}</small>
                                    </div>
                                </div>                            
                            </div>

                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="yourPhone" className="control-label">Your Phone Number</label>
                                        <input type="tel" name="yourPhone" id="yourPhone" className="form-control"
                                            defaultValue={yourPhone} onChange={(input) => setYourPhone(input.target.value)} />
                                        <small className="form-text text-danger">{yourPhoneErr}</small>
                                    </div>
                                </div>                            
                            </div>

                            <hr className="mt-5" />

                            <div className="row">
                                <div className="col">
                                <button className="btn btn-outline-dark px-4" onClick={() => history.push("../create-cover-letter-cover-letter-1/closing-thankyou")}>Back</button>
                                </div>
                                <div className="col text-right">
                                <button className="btn btn-primary px-4" onClick={() => updateContact()}>Preview</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default SignatureSection;