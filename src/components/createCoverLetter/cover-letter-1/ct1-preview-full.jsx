import React from 'react';

import CoverLetterPreview from './ct1-preview'

import '../../buildResume/buildResume.css';


const CoverLetterPreviewPage = () => {

    const routePrefix = '/create-cover-letter';

    return (
        <div className="build-resume-container container-fluid">
            <div className="row">
                <div className="col">
                    <CoverLetterPreview routePrefix={routePrefix} />
                </div>
            </div>
        </div>
        
    );
};

export default CoverLetterPreviewPage;