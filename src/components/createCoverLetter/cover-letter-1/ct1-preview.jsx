import React, { useContext } from 'react';
import { Link, useHistory } from "react-router-dom";
import CoverLetterDoc  from '../../../templates/cover-letter1';
import { PDFDownloadLink, /*PDFViewer*/  } from '@react-pdf/renderer';
import CoverLetter1Html from '../../../templates/cover-letter1-html';
import CoverLetterContext from '../../../providers/cover-letter-context';

const CoverLetter1Preview = () => {

    let history = useHistory();
    const routePrefix = '/create-cover-letter-cover-letter-1';
    const {coverLetterData} = useContext(CoverLetterContext);
    return (
            <>
            {   
                history.location.pathname === "/create-cover-letter-cover-letter-1/preview" ?
                <>
                    <div className="row">
                        <div className="col-md-3">

                            <div className="section-navigation mt-5 px-3">
                                <nav id="side-nav">
                                    <h2>Cover Letter Sections</h2>
                                    <hr/>

                                    <ul style={{lineHeight:3,listStyleType:"none",paddingLeft:"5px"}}>
                                        <li><Link to={routePrefix + "/heading"}><i className="fa fa-align-left" aria-hidden="true"></i> Heading</Link></li>
                                        <li><Link to={routePrefix + "/purpose-of-letter"}><i className="fa fa-file-text-o" aria-hidden="true"></i> Purpose of the Letter</Link></li>
                                        <li><Link to={routePrefix + "/more-about-yourself"}><i className="fa fa-user-o" aria-hidden="true"></i> More About Yourself</Link></li>
                                        <li><Link to={routePrefix + "/closing-thankyou"}><i className="fa fa-envelope-o" aria-hidden="true"></i> Closing and Thank You</Link></li>
                                        <li><Link to={routePrefix + "/signature"}><i className="fa fa-pencil" aria-hidden="true"></i> Signature</Link></li>
                                    </ul>
                                </nav>
                            </div>
                                
                        </div>
                        <div className="col" style={{backgroundColor:'#eee'}}>
                            <div className="preview-controls text-center mt-5 mb-3">
                                <button className="btn btn-outline-dark px-4 btn-sm" onClick={() => history.goBack()}>Back</button>
                                <PDFDownloadLink document={<CoverLetterDoc coverLetterData={coverLetterData}/>} fileName={coverLetterData.yourName + "_coverletter.pdf"}>
                                    {({ blob, url, loading, error }) => (loading ? ' Loading document...' : ' Download PDF')}
                                </PDFDownloadLink>
                            </div>
                            <CoverLetter1Html
                            date = {coverLetterData.heading.date}
                            managerName = {coverLetterData.heading.managerName}
                            companyName = {coverLetterData.heading.companyName}
                            companyAddress1 = {coverLetterData.heading.companyAddress1} 
                            companyAddress2 = {coverLetterData.heading.companyAddress2}
                            letterPurposeParagraph = {coverLetterData.letterPurposeParagraph} 
                            moreAboutYourselfParagraph = {coverLetterData.moreAboutYourselfParagraph ? coverLetterData.moreAboutYourselfParagraph.paragraph: ''}
                            moreAboutYourselfAttributes = {coverLetterData.moreAboutYourselfParagraph ? coverLetterData.moreAboutYourselfParagraph.attributes:[]}
                            sellingYourselfParagraph = {coverLetterData.sellingYourselfParagraph}
                            closingParagraph = {coverLetterData.closingParagraph}
                            thankyouParagraph = {coverLetterData.thankyouParagraph}
                            yourName = {coverLetterData.yourName}
                            yourEmail = {coverLetterData.yourEmail}
                            yourPhone = {coverLetterData.yourPhone}
                            />
                        </div>
                    </div>
                </>
                :
                <>
                    <div className="resume-controls text-center mt-5 mb-3">
                        <span onClick={() => history.push(routePrefix + "/preview")}>
                            <i className="fa fa-eye" aria-hidden="true"></i> Full View
                        </span>
                    </div>
                    <CoverLetter1Html
                    date = {coverLetterData.heading.date}
                    managerName = {coverLetterData.heading.managerName}
                    companyName = {coverLetterData.heading.companyName}
                    companyAddress1 = {coverLetterData.heading.companyAddress1} 
                    companyAddress2 = {coverLetterData.heading.companyAddress2}
                    letterPurposeParagraph = {coverLetterData.letterPurposeParagraph} 
                    moreAboutYourselfParagraph = {coverLetterData.moreAboutYourselfParagraph ? coverLetterData.moreAboutYourselfParagraph.paragraph: ''}
                    moreAboutYourselfAttributes = {coverLetterData.moreAboutYourselfParagraph ? coverLetterData.moreAboutYourselfParagraph.attributes:[]}
                    sellingYourselfParagraph = {coverLetterData.sellingYourselfParagraph}
                    closingParagraph = {coverLetterData.closingParagraph}
                    thankyouParagraph = {coverLetterData.thankyouParagraph}
                    yourName = {coverLetterData.yourName}
                    yourEmail = {coverLetterData.yourEmail}
                    yourPhone = {coverLetterData.yourPhone}
                    />
                </>
            }
            </>
        
    );
};

export default CoverLetter1Preview;