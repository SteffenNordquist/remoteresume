import React from 'react';
import { useHistory } from "react-router-dom";

const CoverLetterTemplates = (props) => {

    const {routePrefix} = props;

    let history = useHistory();

    const updateTemplate = (template) => {
        
        const newRoute = routePrefix + "-" + template;
        history.push(newRoute);
    }

    return (
        <div className="row w-100">
            <div className="col">

                <div className="row text-center">
                    <div className="col">
                        <div className="intro-text my-5">
                            <h1>Choose a template!</h1>
                            <p className="lead">Some additional text about this part</p>
                        </div>
                    </div>
                </div>
                
                <div className="row options text-center">
                    <div className="col-sm-6">
                        <div className="box shadow" onClick={() => updateTemplate('cover-letter-1')}>
                            <h3>Cover Letter 1</h3>
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="box shadow" onClick={() => /*updateTemplate('template2')*/ alert("template under construction")}>
                            <h3>Cover Letter 2</h3>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    );
};

export default CoverLetterTemplates;