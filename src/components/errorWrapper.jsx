import React/*, {useState, useEffect}*/ from 'react';

const ErrorWrapper = (
    {
    options,
    model,
    modelname,
    when,
    children
    }) => {

    var _options = options ? options : 
    {
        noempty: true,
        longtext: false
    }

    var errorMessage = "";
    if (_options.noempty && model === "") {
        errorMessage = modelname + " should not be empty";
    }
    else if(_options.longtext && model.length < 3) {
        errorMessage = "this field is too short";
    }

    return (
        <div>
            {children}
            {errorMessage !== "" && when &&
            <p className="error-text text-red"><small>{errorMessage}</small></p>
            }
        </div>
       
    );
};

export default ErrorWrapper;
