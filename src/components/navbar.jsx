import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import ReactLoader from 'react-loader';
import { useHistory } from "react-router-dom";
import { logout } from "../services/user";
import SignInModal from './signinModal';
import SignUpModal from './signupModal';
import { useSelector, useDispatch } from 'react-redux'
import { signOut, saveIsPremium } from './../actions/index';
import { getCurrentUser } from './../services/user';
import $ from 'jquery';

const Navbar = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.user)
    const [currentUser, setCurrentUser] = useState({})

    let history = useHistory();

    const containerClass = "container";

    useEffect(() => {
        setCurrentUser(getCurrentUser())
    }, [user])

    var timeout;
    const logoutUser = () => {
        $('#signout-modal').modal('show');

        timeout = setTimeout(() => {
            logout()
            dispatch(signOut())
            dispatch(saveIsPremium(false))
            history.push("/")
            $('#signout-modal').modal('hide');
        }, 2000)
    }

    clearTimeout(timeout)

    return (
        <>
            <SignInModal onLog={"asdasd"} />
            <SignUpModal />
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className={containerClass}>
                    <Link className="navbar-brand" to="/">RemoteResume</Link>
                    {/* <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div id="navbar-collapse" className="collapse navbar-collapse"> */}
                    {/* <ul className="navbar-nav ml-auto">
                    <li className="nav-item active">
                        <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Features</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Pricing</a>
                    </li>
                    </ul> */}

                    <div className="ml-auto">
                        {currentUser ?
                            <>
                                <a className="mr-2" href="../profile">My Account</a>
                                <button className="btn btn-primary" onClick={() => logoutUser()}>Sign Out</button>
                            </>
                            :
                            <>
                                <button className="btn btn-outline-primary mr-2" data-toggle="modal" data-target="#signin-modal">Sign In</button>
                                <button className="btn btn-primary" data-toggle="modal" data-target="#signup-modal">Sign Up</button>
                            </>
                        }
                    </div>
                    {/* </div> */}
                </div>
            </nav>

            <div id="signout-modal" className="modal fade signout-modal" tabIndex="-1" role="dialog" aria-labelledby="signout-modal" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <ReactLoader loaded={false} scale={.5} />
                </div>
            </div>
        </>

    );
};

export default Navbar;