import React, { useEffect, useState } from 'react';
import { getResumes } from '../services/user-resumes';
import { getCoverLetters } from '../services/user-coverletters';
import { getUsername } from '../services/encryption';
import { Link } from 'react-router-dom';
import Loader from 'react-loader';

const Resumes = () => {
    const [resumes, setResumes] = useState([]);
    const [coverLetters, setCoverLetters] = useState([]);
    const [loaded, setLoaded] = useState(false);
    const [coverLettersLoaded, setCoverLettersLoaded] = useState(false);

    useEffect(() => {
        fetch();
        fetchCoverLetters();
    }, [])

    const fetch = async () => {
        const { data } = await getResumes(getUsername());
        var _resumes = data;

        _resumes.map((resume, index) => {
            _resumes[index]["resume_content"] = JSON.parse(resume.resume_content);
            _resumes[index]["editing_uri"] = resumeTemplateUrlFactory(resume.resume_template);
            _resumes[index]["type"] = "Resume";
        });

        setResumes(_resumes);
        setLoaded(true);
    }

    const fetchCoverLetters = async () => {
        const { data } = await getCoverLetters(getUsername());
        var _coverLetters = data;
        _coverLetters.map((doc, index) => {
            _coverLetters[index]["content"] = JSON.parse(doc.content);
            _coverLetters[index]["editing_uri"] = coverLetterTemplateUrlFactory(doc.template);
            _coverLetters[index]["type"] = "Cover Letter";
        });
        setCoverLetters(_coverLetters);
        setCoverLettersLoaded(true);
    }

    return (
        <Loader loaded={loaded && coverLettersLoaded} scale={1.0}>
            <div className="row d-none d-lg-flex mb-3">
                <div className="col-7"><strong>Document</strong></div>
                <div className="col-3"><strong>Type</strong></div>
                <div className="col-2"></div>
            </div>
            <>
                {resumes.map((resume, index) => {
                    return (
                        <div className="box box-2 mb-3" key={"resume-" + index}>
                            <div className="row">
                                <div className="col-7 text-capitalize">
                                    {(resume.resume_content.contact.firstname || resume.resume_content.contact.lastname) &&
                                        <span>{resume.resume_content.contact.firstname} {resume.resume_content.contact.lastname}</span>
                                    }
                                    <span className="text-muted mx-2">|</span>
                                    {resume.resume_template}
                                </div>
                                <div className="col-3"><span className="badge badge-success p-2">{resume.type}</span></div>
                                <div className="col-2 text-right"><Link to={resume.editing_uri}><i className="fa fa-pencil" aria-hidden="true"></i> Edit</Link></div>
                            </div>
                        </div>
                    )
                })}
                {coverLetters.map((cl, index) => {
                    return (
                        <div className="box box-2 mb-3" key={"cl-" + index}>
                            <div className="row">
                                <div className="col-7 text-capitalize">
                                    {cl.content.yourName &&
                                        <span>{cl.content.yourName}</span>
                                    }
                                    <span className="text-muted mx-2">|</span>
                                    {cl.template}
                                </div>
                                <div className="col-3"><span className="badge badge-success p-2">{cl.type}</span></div>
                                <div className="col-2 text-right"><Link to={cl.editing_uri}><i className="fa fa-pencil" aria-hidden="true"></i> Edit</Link></div>
                            </div>
                        </div>
                    )
                })}
            </>
        </Loader >
    )
}

const resumeTemplateUrlFactory = (template) => {
    if (template === "template1") {
        return "../build-resume-beginner-template-1";
    }
    return "";
}

const coverLetterTemplateUrlFactory = (template) => {
    if (template === "coverletter1") {
        return "create-cover-letter-cover-letter-1";
    }

    return "";
}

export default Resumes