import React, { useState } from 'react';
import ReactLoader from 'react-loader';
import { useDispatch } from 'react-redux'
import { signIn, saveIsPremium, saveIsVerified } from '../actions'
import { login, isPremium, isVerified } from '../services/user';
import { saveUsername } from '../services/encryption';
import $ from 'jquery';

const SignInModal = (props) => {
    const dispatch = useDispatch()

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [signInErr, setSignInErr] = useState("");
    const [disable, setDisable] = useState(false)

    const loginUser = async () => {
        setDisable(true)
        $('#signout-modal').modal('show');
        if (!(username && password)) {
            setSignInErr("Please complete the fields.");
            return;
        }

        const userData = {
            'username': username,
            'password': password
        };

        let isLogin = false
        try {
            await login(userData);
            dispatch(signIn())

            $('#signin-modal').modal('hide');
            emptyForm();
            saveUsername(username);
            isLogin = true
        } catch (error) {
            const errMessage = error.response.data.message;

            if ((errMessage.toLowerCase().includes("the password you entered for the username") && errMessage.toLowerCase().includes("is incorrect")) ||
                errMessage.includes("Unknown username")) {
                setSignInErr("Username or password is incorrect");
            }
            else {
                setSignInErr(errMessage);
            }
        }
        $('#signout-modal').modal('hide');
        setDisable(false);

        if(isLogin){
            _saveIsPremium(isLogin);
            window.location.href = "/";
        }

        return isLogin
    };

    const emptyForm = () => {
        setUsername("");
        setPassword("");
        setSignInErr("");
    };

    const _saveIsPremium = async (isLogin) => {
        const _isPremium = await isPremium();
        if (isLogin && _isPremium) dispatch(saveIsPremium(true));

        const _isVerified = await isVerified();
        if(isLogin && _isVerified) dispatch(saveIsVerified(true));
    }

    const handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            loginUser().then(isLogin => _saveIsPremium(isLogin));
        }
    }

    const handleSetUsername = (e) => {
        setUsername(e.target.value)

    }

    return <>
        <div id="signin-modal" className="modal fade signin-modal" tabIndex="-1" role="dialog" aria-labelledby="signin-modal" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <h2>Sign In</h2>
                    </div>

                    <div className="modal-body">

                        <div className="form-group col">
                            <label htmlFor="username">Username</label>
                            <input type="text" className="form-control" id="si-username" placeholder="Username" value={username} onChange={handleSetUsername} />
                        </div>
                        <div className="form-group col">
                            <label htmlFor="password">Password</label>
                            <input type="password" className="form-control" id="si-password" placeholder="Password"
                                value={password}
                                onChange={(event) => setPassword(event.target.value)}
                                onKeyPress={(event) => handleKeyPress(event)} />
                        </div>

                    </div>

                    <div className="modal-footer">
                        <small className="text-danger">{signInErr}</small>
                        <button type="button" className="btn btn-secondary" data-dismiss="modal" disabled={disable}>Close</button>
                        <button type="button" className="btn btn-primary" onClick={() => loginUser()} disabled={disable}>Submit</button>
                    </div>

                </div>
            </div>
        </div>
        <div id="signout-modal" className="modal fade signout-modal" tabIndex="-1" role="dialog" aria-labelledby="signout-modal" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <ReactLoader loaded={false} scale={.5} />
            </div>
        </div>
    </>
};

export default SignInModal;