import React, { useState } from 'react';
import ReactLoader from 'react-loader';
import $ from 'jquery';

const SignUpModal = () => {

    const [email, setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [signUpErr, setSignUpErr] = useState("");

    const register = () => {

        $('#loading-modal').modal('show');
        setSignUpErr("");
        emptyForm();

        const domain = "http://admin.remoteresume.co/";

        if (!(username && email && password)) {
            setSignUpErr("Please complete the fields.")
            return;
        }

        const userData = {
            'username': username,
            'email': email,
            'password': password
        };

        $.ajax({
            url: domain + 'wp-json/wp/v2/users/register',
            method: 'POST',
            contentType: "application/json",
            data: JSON.stringify(userData),
            success: function (resp) {
                $('#loading-modal').modal('hide');
                $('#signup-modal').modal('hide');
                emptyForm();
                $('#signin-modal').modal('show');
            },
            error: function (error) {
                setSignUpErr(error.responseJSON.message);
            }
        });
    };

    const emptyForm = () => {
        setEmail("");
        setUsername("");
        setPassword("");
        setSignUpErr("");
    };

    return (
        <>
            <div id="signup-modal" className="modal fade signup-modal" tabIndex="-1" role="dialog" aria-labelledby="signup-modal" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h2>Sign Up</h2>
                        </div>

                        <div className="modal-body">

                            <div className="form-group col">
                                <label htmlFor="username">Username</label>
                                <input type="text" className="form-control" id="username" placeholder="Username" value={username} onChange={(event) => setUsername(event.target.value)} />
                            </div>
                            <div className="form-group col">
                                <label htmlFor="email">Email</label>
                                <input type="email" className="form-control" id="email" placeholder="Email" value={email} onChange={(event) => setEmail(event.target.value)} />
                            </div>
                            <div className="form-group col">
                                <label htmlFor="password">Password</label>
                                <input type="password" className="form-control" id="password" placeholder="Password" value={password} onChange={(event) => setPassword(event.target.value)} />
                            </div>

                        </div>

                        <div className="modal-footer">
                            <p className="text-danger">{signUpErr}</p>
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-primary" onClick={() => register()}>Submit</button>
                        </div>

                    </div>
                </div>
            </div>
            <div id="loading-modal" className="modal fade loading-modal" tabIndex="-1" role="dialog" aria-labelledby="loading-modal" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <ReactLoader loaded={false} scale={.5} />
                </div>
            </div>
        </>
    );
};

export default SignUpModal;