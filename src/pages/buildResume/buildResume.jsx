import React from 'react';
import { Route, Redirect, Switch } from "react-router-dom";

import ExperienceLevel from '../../components/buildResume/experienceLevel';
import BeginnerTemplates from '../../components/buildResume/beginner/begginer-templates';

import '../buildResume/buildResume.css';


const BuildResume = () => {

    const routePrefix = '/build-resume';

    return (
        <div className="build-resume-container container-fluid">
            <div className="row">
                <div className="col build-resume-form">
                    <div className="container">
                    
                        <div className="content">
                        
                            <div className="row mt-5">
                                <div className="col text-center">
                                    <h1 className="display-4">Build Resume 
                                    </h1>

                                    {/* <div className="controls">
                                    {history.location.pathname == "/build-resume/preview" ?
                                    <div className="preview-controls">
                                        <button className="btn btn-outline-dark px-4 btn-sm" onClick={() => history.goBack()}>Back</button>
                                    </div>
                                    :
                                    <div className="resume-controls">
                                        <span onClick={() => history.push(routePrefix + "/preview")}>
                                            <i className="fa fa-eye" aria-hidden="true"></i> preview
                                        </span>
                                    </div>
                                    }
                                    </div> */}
                                    
                                    
                                </div>
                            </div>
                            
                            <div className="row">
                                <div className="build-resume mb-5 col">
                                    <div className="center-xy">
                                        <Switch>
                                            {/* <Route path={routePrefix + "/preview"} render={() => <ResumePreview routePrefix={routePrefix} />}></Route> */}
                                            <Route path={routePrefix + "/experience-level"} render={() => <ExperienceLevel routePrefix={routePrefix} />}></Route>
                                            <Route path={routePrefix + "/beginner-templates"} render={() => <BeginnerTemplates routePrefix={routePrefix} />}></Route>
                                            <Redirect from={routePrefix} to={routePrefix + "/experience-level"}></Redirect>
                                        </Switch>
                                    </div>
                                </div>
                            </div>
                        

                        </div>
                        
                    </div>
                </div>
                
            </div>
        </div>
        
    );
};

export default BuildResume;