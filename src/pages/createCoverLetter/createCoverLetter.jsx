import React from 'react';
import { Route, Redirect, Switch } from "react-router-dom";
import CoverLetterTemplates from '../../components/createCoverLetter/cover-letter-templates';

import '../buildResume/buildResume.css';


const CreateCoverLetter = () => {

    const routePrefix = '/create-cover-letter';

    return (
        <div className="build-resume-container container-fluid">
            <div className="row">
                <div className="col build-resume-form">
                    <div className="container">
                    
                        <div className="content">
                        
                            <div className="row mt-5">
                                <div className="col text-center">
                                    <h1 className="display-4">Create Cover Letter
                                    </h1>
                                </div>
                            </div>
                            
                            <div className="row">
                                <div className="build-resume mb-5 col">
                                    <div className="center-xy">
                                        <Switch>
                                            <Route path={routePrefix + "/cover-letter-templates"} render={() => <CoverLetterTemplates routePrefix={routePrefix} />}></Route>
                                            <Redirect from={routePrefix} to={routePrefix + "/cover-letter-templates"}></Redirect>
                                        </Switch>
                                    </div>
                                </div>
                            </div>
                        

                        </div>
                        
                    </div>
                </div>
                
            </div>
        </div>
        
    );
};

export default CreateCoverLetter;