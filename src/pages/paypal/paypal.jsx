import React, { useState, useRef, useEffect } from 'react';
import { savePaymentDetails } from '../../services/payment';
import { getUsername } from '../../services/encryption';
import LinkButton from '../../components/linkButton';
import './paypal.css'
import { useDispatch } from 'react-redux';
import { saveIsPremium } from '../../actions';

const Paypal = () => {
    const [paidFor, setPaidFor] = useState(false)
    let paypalRef = useRef()
    const dispatch = useDispatch()

    const product = {
        price: 27.00,
        description: 'Remote Resume - 1 month membership'
    }

    const dtLocalString = new Date(Date.now()).toLocaleString();
    const dtNow = dtLocalString.toLocaleString().split(', ')[0]

    var next30Days = new Date(+new Date() + 30 * 86400000)
    const dtNext30Days = next30Days.toLocaleString().split(', ')[0]

    const renderCongrats = () => {
        return <>
            <div className="message">
                <h2>Congratulations</h2>
                <h4>You are now a premium member.</h4>

                <LinkButton className="btn btn-primary btn-lg mt-3" to="/profile">Go to your profile</LinkButton>
            </div>
        </>
    }

    const renderForm = () => {
        return <>
            <main className="page payment-page">
                <section className="payment-form dark">
                    <div className="container">
                        <div className="block-heading">
                            <h2>Membership</h2>
                            <p>Love our resume? Own it now!</p>
                        </div>
                        <form>
                            <div className="products">
                                <h3 className="title">Checkout</h3>
                                <div className="item">
                                    <span className="price">${product.price}</span>
                                    <p className="item-name">{product.description}</p>
                                    <p className="item-description">Membership period from <span className="font-weight-bold">{dtNow} to {dtNext30Days}</span></p>
                                </div>
                                <div className="total"><h3>Total <span className="big-price">$27</span></h3></div>
                            </div>

                            <div className="card-details">
                                <h3 className="title">Pay with the following</h3>
                                <div className="row d-flex justify-content-center mt-5">
                                    <div className="col-md-5">
                                        <div ref={v => (paypalRef = v)}></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </main>
        </>
    }

    useEffect(() => {
        window.paypal
            .Buttons({
                createOrder: (data, actions) => {
                    return actions.order.create({
                        purchase_units: [
                            {
                                description: product.description,
                                amount: {
                                    currency_code: 'USD',
                                    value: product.price,
                                }
                            }
                        ]
                    })
                },
                onApprove: async (data, actions) => {
                    const order = await actions.order.capture()

                    setPaidFor(true)

                    const userName = getUsername()
                    savePaymentDetails(userName, order)

                    dispatch(saveIsPremium(true))
                }
            }).render(paypalRef)
    })

    return paidFor ? renderCongrats() : renderForm()

}

export default Paypal;