import React from 'react';
import Resumes from '../components/resumes';
import LinkButton from '../components/linkButton';
import { useSelector } from 'react-redux';

const Profile = () => {
    const state = useSelector(state => state);
    
    return <>
        {state.user && <div className="container">
            <section className="row">
                <div className="col">

                    <h1>My Account</h1>
                    {state.verifiedUser ?
                        <p>Status: <b className="text-success">Verified</b></p> : 
                        <div>
                            <p>Status: <b className="text-danger">Not Verified</b></p>
                            <LinkButton className="btn btn-primary btn-xs" to="/verify-account">Verify Account</LinkButton>
                        </div>
                    }
                    
                    <hr />
                    <div className="text-right mb-5">
                        {state.premiumUser && <LinkButton className="btn btn-primary" to="/home-banner">Create New</LinkButton>}
                    </div>

                    <Resumes />

                    {!state.premiumUser && 
                        <>
                            <br></br>
                            <LinkButton className="btn btn-lg btn-primary" to="/membership">Pay Monthly Membership Now!</LinkButton>
                        </>
                    }
                </div>
            </section>
        </div>}

    </>
};

export default Profile;