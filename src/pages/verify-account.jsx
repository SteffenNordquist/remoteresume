import React, {useState} from 'react';
import { useHistory } from "react-router-dom";
import { useDispatch } from 'react-redux';
import {verifyAccount} from '../services/user';
import { saveIsVerified } from '../actions';

const VerifyAccount = () => {
    
    let history = useHistory();
    const dispatch = useDispatch();

    const [pin, setPin] = useState("");
    const [pinErr, setPinErr] = useState("");

    const verify = () =>{

        setPinErr("");

        if(!pin)
        {
            setPinErr("PIN required");
            return;
        }

        const req = async () => await verifyAccount(pin);
        req().then(({data}) => {
            if(data && data.res && data.res.data === 1){
                dispatch(saveIsVerified(true));
                history.push("/profile");
            }
            else{
                setPinErr("Invalid PIN");
            }
        });

    }
    
    return <>
        <div className="container">
            <section className="row">
                <div className="col">
                <h2>Verify Account</h2>
                <p>Verification PIN was sent to your email</p>
                <div className="row">
                    <div className="col-sm-4">
                        <div className="form-group">
                            <label htmlFor="pin" className="control-label">Enter verification PIN</label>
                            <input type="text" name="pin" id="pin" className="form-control"
                                defaultValue={pin} onChange={(input) => setPin(input.target.value)} />
                            <small className="form-text text-danger">{pinErr}</small>
                        </div>
                    </div>
                </div>
                <div>
                    <button className="btn btn-primary" onClick={() => verify()}>Verify</button>
                </div>
                </div>
            </section>
        </div>

    </>
};

export default VerifyAccount;