import React from 'react';

const CoverLetterContext = React.createContext();

export default CoverLetterContext;