import {getCoverLetterData, saveCoverLetterData} from '../services/persistence';

const saveCoverLetterDataThis = (template, content) =>{
    saveCoverLetterData(template, content);
    let newContent = content;
    setTimeout(() => {
        newContent = async() => {return await getCoverLetterData(template)}
    },1000);
    return newContent;
}

export const coverLetterReducer = (state, action) =>{
    switch(action.type){
        case 'SAVE_COVER_LETTER':
            return saveCoverLetterDataThis(action.template, state);
        default:
            return state;
    }
};