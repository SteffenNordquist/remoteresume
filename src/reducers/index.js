import user from "./user"
import premiumUser from './premiumUser'
import verifiedUser from './verifiedUser';
import { combineReducers } from 'redux'

const reducers = combineReducers({ user, premiumUser, verifiedUser })

export default reducers