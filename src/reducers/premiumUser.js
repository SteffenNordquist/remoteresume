const premiumUser = (state = false, action) => {
    switch (action.type) {
        case 'IS_PREMIUM':
            return action.isPremium

        default:
            return state;
    }
};

export default premiumUser