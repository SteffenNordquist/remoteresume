import {getResumeData, saveResumeData} from '../services/persistence';

const saveResumeDataThis = (experience, template, resumeData) =>{
    saveResumeData(experience,template, resumeData);
    let newResumeData = resumeData;
    setTimeout(() => {
        newResumeData = async() => {return await getResumeData(experience, template)}
    },1000);
    return newResumeData;
}

export const resumeReducer = (resumeState, action) =>{
    switch(action.type){
        case 'SAVE_RESUME':
            return saveResumeDataThis(action.experience, action.template, resumeState);
        default:
            return resumeState;
    }
};