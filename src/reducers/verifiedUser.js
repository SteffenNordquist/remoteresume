const verifiedUser = (state = false, action) => {
    switch (action.type) {
        case 'IS_VERIFIED':
            return action.isVerified

        default:
            return state;
    }
};

export default verifiedUser