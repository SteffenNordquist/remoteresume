import CryptoJS from 'crypto-js';

const key = "Skjk&923%$2k9kSKJk1a0082";

export function saveUsername(username){
    localStorage.setItem("emanresu", CryptoJS.AES.encrypt(username,key));
}

export function getUsername(){
    const emanresu = localStorage.getItem("emanresu");
    if(!emanresu)
        return "";

    var bytes  =  CryptoJS.AES.decrypt(emanresu.toString(),key);
    var plaintext = bytes.toString(CryptoJS.enc.Utf8);
    return plaintext;
}