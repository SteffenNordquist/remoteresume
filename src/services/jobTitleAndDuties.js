import http from './http'

export async function getJobTitleAndDuties() {
    const path = '/wp-json/resumes/jobtitles/v1/get'
    return await http.get(path)
};