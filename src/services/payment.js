import http from './http'

const baseAPIURL = 'http://admin.remoteresume.co/'

export async function savePaymentDetails(userId, payment_details) {
    const path = baseAPIURL + `wp-json/payment/v1/Snc146IbI4z04Ci2OE9cgmPD1AvDpmKA?userid=${userId}`
    const payload = {
        payment_details,
        secret_key: "H3PEth5O3sS6rIDnkVOnQYV4Ua4EPV8Qr7PCeifkjP13jKYCGVV5xOQsYb2vqe5Q"
    }

    return await http.post(path, payload)
};

export async function getNextPayment(userId) {
    const path = baseAPIURL + `wp-json/nextpayment/v1/Snc146IbI4z04Ci2OE9cgmPD1AvDpmKA?userid=${userId}`

    return await http.get(path)
}