import {getResume, /*getResumes,*/ saveUserResume} from '../services/user-resumes';
import {getCoverLetter, /*getCoverLetters,*/ saveUserCoverLetter} from '../services/user-coverletters';
import {getUsername} from '../services/encryption';

export function getResumeData(experience, template){
    
    var username = getUsername();
    if(!username){
        var resumeData = localStorage.getItem("resumeData");
        if (!resumeData) {
            resumeData = defaultResume();
            localStorage.setItem("resumeData", JSON.stringify(resumeData));
        }
        return createPromise(JSON.parse(resumeData));
    }
    else{
        return getResume(username, experience, template);
    }
    
}

export function saveResumeData(experience, template, resumeData){
    var username = getUsername();
    if(!username){
        localStorage.setItem("resumeData", JSON.stringify(resumeData));
    }
    else{
        const saveResumeReq = async () => await saveUserResume(username, experience, template, resumeData);
        saveResumeReq().then(data => {/*console.log(data.data)*/});
    }
}

export function defaultResume(){
    const resumeDataDefault = {
        contact:{
            firstname: '',
            lastname: '',
            city: '',
            state: '',
            zipcode: '',
            phone: '',
            email: '',
            linkedIn: ''
        },
        objective: {},
        educations: [],
        awards: {},
        jobExperiences: []
    };
    return resumeDataDefault;
}

export function getCoverLetterData(template){
    
    var username = getUsername();
    if(!username){
        var coverLetterData = localStorage.getItem("coverLetterData");
        if (!coverLetterData) {
            coverLetterData = defaultCoverLetter1();
            localStorage.setItem("coverLetterData", JSON.stringify(coverLetterData));
        }
        return createPromise(JSON.parse(coverLetterData));
    }
    else{
        return getCoverLetter(username, template);
    }
    
}

export function saveCoverLetterData(template, content){
    var username = getUsername();
    if(!username){
        localStorage.setItem("coverLetterData", JSON.stringify(content));
    }
    else{
        const saveReq = async () => await saveUserCoverLetter(username, template, content);
        saveReq().then(data => {/*console.log(data.data)*/});
    }
}

export function defaultCoverLetter1(){
    const letterDoc = {       
        heading:{
            date:'',
            managerName:'',
            companyName:'',
            companyAddress1:'',
            companyAddress2:''
        },
        letterPurposeParagraph:'',
        moreAboutYourselfParagraph:{
            paragraph:'',
            attributes:[]
        },
        sellingYourselfParagraph:'',
        closingParagraph:'',
        thankyouParagraph:'',
        yourName:'',
        yourEmail:'',
        yourPhone:''
    }
    return letterDoc;
}

const createPromise = (data) => {
    var promise = new Promise(function(resolve, reject) {
        resolve(data);
      });
    return promise;
}