import http from "./http";

export function getCoverLetters(userid) {
    return http.get("http://admin.remoteresume.co/wp-json/coverletters/v1/get?userid=" + userid)
}

export function getCoverLetter(userid,template) {
    return http.get(`http://admin.remoteresume.co/wp-json/coverletters/v1/get?userid=${userid}&template=${template}`)
}

export function saveUserCoverLetter(userid, template, content){

    const config = {
        headers: {
          'Content-Type': 'application/json'
        }
    }

    const requestBody = {
        content: content
    }
    
    return http.post(
        `http://admin.remoteresume.co/wp-json/coverletters/v1/save?userid=${userid}&&template=${template}`, 
        requestBody,
        config)
}