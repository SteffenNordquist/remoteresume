import http from "./http"

export function getResumes(userid) {
    return http.get("http://admin.remoteresume.co/wp-json/resumes/v1/get?userid=" + userid)
}

export function getResume(userid,experience,template) {
    return http.get(`http://admin.remoteresume.co/wp-json/resumes/v1/get?userid=${userid}&resume_experience_level=${experience}&resume_template=${template}`)
}

export function saveUserResume(userid, experienceLevel, template, resumeJson){

    const config = {
        headers: {
          'Content-Type': 'application/json'
        }
    }

    const requestBody = {
        resume_content: resumeJson
    }
    
    return http.post(
        `http://admin.remoteresume.co/wp-json/resumes/v1/save?userid=${userid}&resume_experience_level=${experienceLevel}&resume_template=${template}`, 
        requestBody,
        config)
}

// export function updateUserResume(userid, experienceLevel, template, resumeJson){

//     const config = {
//         headers: {
//           'Content-Type': 'application/x-www-form-urlencoded'
//         }
//     }

//     const requestBody = {
//         resume_content: resumeJson
//     }

//     return http.post(
//         `http://admin.remoteresume.co/wp-json/resumes/v1/update?userid=${userid}&resume_experience_level=${experienceLevel}&resume_template=${template}`, 
//         qs.stringify(requestBody),
//         config)
// }