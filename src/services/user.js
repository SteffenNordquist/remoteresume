import jwtDecode from "jwt-decode";
import http from "../services/http"
import { getUsername } from "./encryption";
import { getNextPayment } from "./payment";

const domain = "http://admin.remoteresume.co/";

export async function login(userData) {
    const path = "wp-json/jwt-auth/v1/token"
    const { data } = await http.post(domain + path, userData)

    if (data)
        localStorage.setItem("token", JSON.stringify(data.token))

    return data;
};

export function logout() {
    localStorage.removeItem("token");
    localStorage.removeItem("emanresu");
}

export function getCurrentUser() {
    try {
        const jwt = localStorage.getItem("token")
        return jwtDecode(jwt)
    } catch (error) {
        return null
    }
}

export async function isPremium() {
    const userId = getUsername()
    if (!userId) return

    const { data: nextPayment } = await getNextPayment(userId)

    var dtNow = new Date();
    const dtNextPayment = new Date(nextPayment)

    return dtNow < dtNextPayment
}

export async function isVerified(){

    const userId = getUsername();
    if (!userId) return;

    const path = "wp-json/isuserverified/v1/Snc146IbI4z04Ci2OE9cgmPD1AvDpmKA?userid=" + userId;

    const {data} = await http.get(domain + path);
    
    return data;

}

export async function verifyAccount(pin){

    const userId = getUsername();
    if (!userId) return;

    const config = {
        headers: {
          'Content-Type': 'application/json'
        }
    }

    const requestBody = {
        rrsecret123_userid: userId,
        rrsecret123_verification_pin: pin
    }
    
    const path = "wp-json/userverify/v1/Snc146IbI4z04Ci2OE9cgmPD1AvDpmKA";

    return http.post(
        domain + path, 
        requestBody,
        config);
}