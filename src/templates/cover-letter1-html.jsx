import React from 'react';

const CoverLetter1 = (props) => {
    
    const date = props.date;
    const managerName = props.managerName ? props.managerName : "";
    const companyName = props.companyName ? props.companyName : "";
    const companyAddress1 = props.companyAddress1 ? props.companyAddress1 : "";
    const companyAddress2 = props.companyAddress2 ? props.companyAddress2 : "";
    const letterPurposeParagraph = props.letterPurposeParagraph ? props.letterPurposeParagraph : "";
    const moreAboutYourselfParagraph = props.moreAboutYourselfParagraph ? props.moreAboutYourselfParagraph : "";
    const moreAboutYourselfAttributes = props.moreAboutYourselfAttributes ? props.moreAboutYourselfAttributes : [];
    const sellingYourselfParagraph = props.sellingYourselfParagraph ? props.sellingYourselfParagraph : "";
    const closingParagraph = props.closingParagraph ? props.closingParagraph : "";
    const thankyouParagraph = props.thankyouParagraph ? props.thankyouParagraph : "";
    const yourName = props.yourName ? props.yourName : "";
    const yourEmail = props.yourEmail ? props.yourEmail : "";
    const yourPhone = props.yourPhone ? props.yourPhone : "";
    
    const dearManager = managerName ? "Dear " + managerName + ":": "";
    const sincerely = yourName || yourEmail || yourPhone ? "Sincerely," : "";

    const container = {
        width: '682px',
        minHeight: '965px',
        margin: '0 auto',
        marginBottom: '50px'
    };
    const paper = {
        height: '100%',
        paddingRight: '15px',
        paddingLeft: '15px',
        marginRight: 'auto',
        marginLeft: 'auto',
        backgroundColor: '#fff',
        padding: '30px'
    };
    const row = {
        display: '-ms-flexbox',
        display: 'flex',
        flexWrap: 'wrap',
        marginRight: '-15px',
        marginLeft: '-15px',
    };
    const col = {
        flexBasis: '0',
        flexGrow: '1',
        maxWidth: '100%',
        position: 'relative',
        width: '100%',
        paddingRight: '15px',
        paddingLeft: '15px'
    };

    const textLeft = {
        textAlign: 'left',
    };
    const textJustify = {
        textAlign: 'justify',
    };
    
    return (
    
        <div style={container}>
            <div style={paper}>
                <div style={row}>
                    <div style={col}>
                        <div style={textLeft}>
                            <p>{date}</p>                           
                        </div>
                        <div style={textLeft}>
                            <p>
                                {managerName}<br/>
                                {companyName}<br/>
                                {companyAddress1}<br/>
                                {companyAddress2}
                            </p>                           
                        </div>
                        <div style={textLeft}>
                            <p>{dearManager}</p>
                            <p style={textJustify}>{letterPurposeParagraph}</p>
                            <p style={textJustify}>{moreAboutYourselfParagraph}</p>
                            <ul style={textLeft}>
                                {moreAboutYourselfAttributes.map((attribute, index) => (
                                    <li key={"attribute"+Math.random()}>{attribute}</li>
                                ))}
                            </ul>
                            <p>{sellingYourselfParagraph}</p>  
                            <p>{closingParagraph}</p>  
                            <p>{thankyouParagraph}</p>                       
                        </div>
                        <div style={textLeft}>
                        <p>{sincerely}</p>                           
                        </div>                       
                        <div style={textLeft}>
                            <p>
                                {yourName}<br/>
                                <a href="#">{yourEmail}</a><br/>
                                {yourPhone}<br/>
                            </p>                           
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    );
};

export default CoverLetter1;