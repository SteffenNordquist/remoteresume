import React from 'react';
import { Document, Page, Text, View, StyleSheet, Font } from '@react-pdf/renderer';

Font.register({ family: 'KadwaRegular', src:'https://fonts.gstatic.com/s/kadwa/v2/rnCm-x5V0g7ipiTAT8M.ttf' } );
Font.register({ family: 'KadwaBold', src:'https://fonts.gstatic.com/s/kadwa/v2/rnCr-x5V0g7ipix7atM5kn0.ttf' } );
Font.register({ family: 'MontserratRegular', src:'https://fonts.gstatic.com/s/montserrat/v12/JTUSjIg1_i6t8kCHKm459Wlhzg.ttf' } );

const styles = StyleSheet.create({
    bold900:{
        fontWeight:900,
        fontFamily:'KadwaBold'
    },
    contentCenter:{
        textAlign: 'center'
    },
    contentLeft:{
        fontSize:12,
        flex: 1,
        textAlign: 'left'
    },
    contentRight:{
        fontSize:12,
        flex: 1,
        textAlign: 'right'
    },
    marginTop30:{
        marginTop: 30
    },
    marginBottom30:{
        marginBottom: 30
    },
    page: { 
        backgroundColor: '#fff',
        paddingLeft:70, 
        paddingRight:70,
        fontFamily:'KadwaRegular'
    },
    sectionheader: { 
        color: 'black', 
        textAlign: 'left', 
        marginTop: 50,
        marginBottom: 10,
        fontSize:12, 
        fontWeight:700
    },
    subsequentsectionheader: { 
        color: 'black', 
        textAlign: 'left', 
        marginTop: 10, 
        fontSize:12, 
        fontWeight: 'bold'
      },
    sectioncontent: { 
        color: 'black', 
        textAlign: 'left', 
        fontSize:12
      },
     sectioncontent_attrib: { 
       color: 'black', 
       textAlign: 'left', 
       fontSize:12,
       paddingLeft:17
     },
    sectioncontent_email: { 
        color: 'blue', 
        textAlign: 'left', 
        textDecoration: 'underline',
        fontSize:12
      }
  });
  
const CoverLetterDoc = ({coverLetterData}) =>{
    
    const letterDoc = coverLetterData;  
    return (
        <Document>
        <Page size="A4" style={styles.page}>

            <View wrap={false} style={styles.marginBottom30}>
                <Text style={styles.sectionheader}>{letterDoc.heading.date}</Text>
                <Text style={styles.sectioncontent}>{letterDoc.heading.managerName}</Text>
                <Text style={styles.sectioncontent}>{letterDoc.heading.companyName}</Text>
                <Text style={styles.sectioncontent}>{letterDoc.heading.companyAddress1}</Text>
                <Text style={styles.sectioncontent}>{letterDoc.heading.companyAddress2}</Text>     
                <Text style={styles.subsequentsectionheader}>Dear {letterDoc.heading.managerName}:</Text>
                <View wrap={false}>
                    <Text style={styles.subsequentsectionheader}>{letterDoc.letterPurposeParagraph}</Text>
                </View>
                <View wrap={false}>
                    <Text style={styles.subsequentsectionheader}>{letterDoc.moreAboutYourselfParagraph.paragraph}</Text>
                </View>
                <Text style={styles.subsequentsectionheader}></Text>
                {letterDoc.moreAboutYourselfParagraph.attributes.length>0 && letterDoc.moreAboutYourselfParagraph.attributes.map((attribute, index) => 
                    (
                        <Text key={"prof-org-"+Math.random()} style={styles.sectioncontent_attrib}>   •   {attribute}</Text>
                    ))
                }
                <View wrap={false}>
                    <Text style={styles.subsequentsectionheader}>{letterDoc.sellingYourselfParagraph}</Text>
                </View>
                <View wrap={false}>
                    <Text style={styles.subsequentsectionheader}>{letterDoc.closingParagraph}</Text>
                </View>
                <Text style={styles.subsequentsectionheader}>{letterDoc.thankyouParagraph}</Text>            
                <Text style={styles.subsequentsectionheader}>Sincerely,</Text>
                <Text style={styles.subsequentsectionheader}>{letterDoc.yourName}</Text>
                <Text style={styles.sectioncontent_email}>{letterDoc.yourEmail}</Text>
                <Text style={styles.sectioncontent}>{letterDoc.yourPhone}</Text> 
            </View>  
        </Page>
      </Document>
        );
}

export default CoverLetterDoc;