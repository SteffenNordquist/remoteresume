import React from 'react';
import { Document, Page, Text, View, StyleSheet, Font } from '@react-pdf/renderer';

const Template1 = (props) => {

    const firstname = props.firstname ? props.firstname : "";
    const lastname = props.lastname ? props.lastname : "";
    const contactCity = props.contactCity ? props.contactCity : "";
    const contactState = props.contactState ? props.contactState : "";
    const contactZip = props.contactZip ? props.contactZip : "";
    const contactPhone = props.contactPhone ? " | C: " + props.contactPhone : "";
    const contactEmail = props.contactEmail ? props.contactEmail : "";
    const precontactEmail = contactEmail ? " | E: " : "";
    const contactLinkedIn = props.linkedIn ? props.linkedIn : "";
    const precontactLinkedIn = contactLinkedIn ? " | " : "";

    let contactAddress = `${contactZip} ${contactCity} ${contactState}`;
    contactAddress = contactAddress ?  contactAddress : "";

    const objectiveHeading = props.objective.heading ? props.objective.heading : "";
    const objectiveSummary = props.objective.summary ? props.objective.summary : "";
    const objectiveObjective = props.objective.objective ? props.objective.objective : "";
    const objectiveProfileItems = props.objective.profileItems ? props.objective.profileItems : [];

    const educations = props.educations ? props.educations : [];
    // const educSchool = props.educSchool ? props.educSchool : "College/University";
    // const educDegree = props.educDegree ? props.educDegree : "Bachelor of Degree";
    // const educCity = props.educCity ? props.educCity : "City";
    // const educState = props.educState ? props.educState : "State";
    // const educGradDate = props.educGradDate ? props.educGradDate : "Date";

    const awardsHeading = props.awards.heading ? props.awards.heading : "";
    const awardsOrgs = props.awards.organizations ? props.awards.organizations : [];
    const awardsAwards = props.awards.awards ? props.awards.awards : [];

    const jobExperiences = props.jobExperiences ? props.jobExperiences : [];
    // const jobExpJobTitle = props.jobExpJobTitle ? props.jobExpJobTitle : "Job Title";
    // const jobExpEmployer = props.jobExpEmployer ? props.jobExpEmployer : "Company Name";
    // const jobExpCity = props.jobExpCity ? props.jobExpCity : "City";
    // const jobExpState = props.jobExpState ? props.jobExpState : "State";
    // const jobExpStart = props.jobExpStart ? props.jobExpStart : "Date";
    // const jobExpEnd = props.jobExpEnd ? props.jobExpEnd : "Date";
    // const jobExpDescription = props.jobExpDescription ? props.jobExpDescription : "";

    const container = {
        width: '682px',
        minHeight: '965px',
        margin: '0 auto',
        marginBottom: '50px'
    };
    const paper = {
        height: '100%',
        paddingRight: '15px',
        paddingLeft: '15px',
        marginRight: 'auto',
        marginLeft: 'auto',
        backgroundColor: '#fff',
        padding: '30px'
    };
    const row = {
        display: '-ms-flexbox',
        display: 'flex',
        flexWrap: 'wrap',
        marginRight: '-15px',
        marginLeft: '-15px',
    };
    const col = {
        flexBasis: '0',
        flexGrow: '1',
        maxWidth: '100%',
        position: 'relative',
        width: '100%',
        paddingRight: '15px',
        paddingLeft: '15px'
    };
    const col6 = {
        flex: '0 0 50%',
        maxWidth: '50%',
    };

    const colTextRight = {
        flexBasis: '0',
        flexGrow: '1',
        maxWidth: '100%',
        position: 'relative',
        width: '100%',
        paddingRight: '15px',
        paddingLeft: '15px',
        textAlign: 'right'
    }

    const textCenter = {
        textAlign: 'center',
    };
    const textRight = {
        textAlign: 'right',
    };

    const mb0 = {
        marginBottom: '0px',
    };
    const mt0 = {
        marginTop: '0px',
    };

    const adam = {
        fontFamily: 'Arial, Helvetica, sans-serif',
        padding: '30px',
        border: '5px solid #666',
        marginTop: '30px'
    };
    
    return (
    
        <div style={container}>
            <div style={paper}>
                <div style={row}>
                    <div style={col}>
                        <div style={textCenter}>
                            <p><strong>{firstname} {lastname}</strong></p>
                            <p>{contactAddress} 
                                {contactPhone} 
                                {precontactEmail}<a href="#">{contactEmail}</a> 
                                {precontactLinkedIn}{contactLinkedIn}
                            </p>
                        </div>
                    </div>
                </div>
{/* OBJECTIVE */}
                <div style={row}>
                    <div style={col}>
                        <div style={textCenter}>
                            <p style={{textTransform:"capitalize"}}><strong>{objectiveHeading}</strong></p>
                            {objectiveHeading == "profile" &&
                            <ul style={{textAlign:"left"}}>
                                {objectiveProfileItems.map((item, index) => (
                                    <li key={"profile"+Math.random()}>{item}</li>
                                ))}
                            </ul>
                            }
                            {objectiveHeading == "summary" &&
                            <p>{objectiveSummary}</p>
                            }
                            {objectiveHeading == "objective" &&
                            <p>{objectiveObjective}</p>
                            }
                        </div>
                    </div>
                </div>
{/* EDUCATION */}
                <div style={row}>
                    <div style={col}>

                        <div style={row, textCenter}>
                            <div style={col}>
                                <p><strong>{educations.length > 0 ? "Education" : ""}</strong></p>
                            </div>
                        </div>

                        {/* Eduction Placeholder */}
                        {/* {educations.length == 0 &&
                        <>
                        <div style={row}>
                            <div style={col}>
                                <p style={mb0}><strong>College/University</strong></p>
                                <p style={mt0}>Bachelor of Degree</p>
                            </div>
                            <div style={colTextRight}>
                                <p style={mb0}>City, State</p>
                                <p style={mt0}>Graduation Date</p>
                            </div>
                        </div>
                        <p>Coursework in class 1, class 2, and class 3</p>
                        </>
                        } */}

                        {educations.map((education, index) => (
                        <div key={'school'+Math.random()}>
                            <div style={row}>
                                <div style={col}>
                                    <p style={mb0}><strong>{education.school ? education.school : "College/University"}</strong></p>
                                    <p style={mt0}>
                                        <span>{education.degree ? education.degree : "Bachelor of"} </span>
                                        {education.studyField &&<span>in </span>}
                                        <span>{education.studyField ? education.studyField : "Degree"} </span>
                                    </p>
                                </div>
                                <div style={colTextRight}>
                                    <p style={mb0}>{education.city ? education.city : "City"}, {education.state ? education.state : "State"}</p>
                                    <p style={mt0}>{education.graduateDate ? education.graduateDate : "Date"}</p>
                                </div>
                            </div>

                            <div style={row}>
                                <div style={col}>
                                    <p>
                                    {!education.classes &&
                                    <span>Coursework in class 1, class 2, and class 3</span>
                                    }
                                    {education.classes.length > 0 &&
                                    <><strong>Coursework</strong><br/></>
                                    }
                                    {education.classes.length > 0 &&
                                    education.classes.map((schoolClass, index) => (
                                    <><span key={'class'+Math.random()}>{schoolClass}</span><br/></>
                                    ))}
                                    </p>
                                </div>
                            </div>
                            <br/>
                        </div>
                        ))}
                        
                    </div>
                </div>
{/* AWARDS */}
                <div style={row}>
                    <div style={col}>
                        <p style={textCenter}><strong style={{textTransform:"capitalize"}}>{awardsHeading}</strong></p>

                        {awardsHeading == "organizations" && awardsOrgs &&
                        <ul style={{textAlign:"left"}}>
                            {awardsOrgs.map((award, index) => (
                                <li style={mb0} key={'award'+Math.random()}>{award}</li>
                            ))}
                        </ul>
                        } 

                        {awardsHeading == "awards / accomplishments" && awardsAwards &&
                        <ul style={{textAlign:"left"}}>
                            {awardsAwards.map((award, index) => (
                                <li style={mb0} key={'award'+Math.random()}>{award}</li>
                            ))} 
                        </ul>
                        }

                        
                        <br/>
                    </div>
                </div>

{/* JOB EXPERIENCE */}
                <div style={row}>
                    <div style={col}>
                        <p style={textCenter}><strong>{jobExperiences.length > 0 ? "Job Experience" : ""}</strong></p>
                        
                        <div style={row}>
                            <div style={col}>

                                {jobExperiences.length == 0 ?
                                <>
                                {/* Job Experience Placeholder */}
                                {/* <div style={row}>
                                    <div style={col}>
                                        <p>Job title, <strong>Company Name</strong></p>
                                    </div>
                                    <div style={colTextRight}>
                                        <p style={mb0}>Date to Date</p>
                                        <p style={mt0}>City, State</p>
                                    </div>
                                </div>
        
                                <div style={row}>
                                    <div style={col}>
                                        <p>You can add a brief 1-2 line description about the company + what you did while there.</p>
                                        <p>
                                        .Managed...<br/>
                                        .Developed...<br/>
                                        .Traveled to...<br/>
                                        .Conducted...<br/>
                                        .Analyzed...<br/>
                                        </p>    
                                    </div>
                                </div> */}
                                </>
                                :
                                <>
                                    {jobExperiences.map((experience, index) => (
                                    <div key={'jobex'+Math.random()}>
                                        <div style={row}>
                                            <div style={col}>
                                                <p>{experience.jobTitle}, <strong>{experience.employer}</strong></p>
                                            </div>
                                            
                                            <div style={colTextRight}>
                                                {experience.employmentStart && experience.employmentEnd &&
                                                    <p style={mb0}>{experience.employmentStart} to {experience.employmentEnd}</p>}

                                                {experience.city && experience.state &&
                                                    <p style={mt0}>{experience.city}, {experience.state}</p>}
                                            </div>
                                           
                                        </div>
                
                                        <div style={row}>
                                            <div style={col}>
                                                <p>{experience.description}</p>    
                                            </div>
                                        </div>

                                        <div style={row}>
                                            <div style={col}>
                                                <ul>
                                                    {experience.duties && experience.duties.map((duty, index) => (
                                                        <li key={'duty'+Math.random()}>{duty}</li>
                                                    ))}
                                                </ul>   
                                            </div>
                                        </div>
                                        <br/>
                                    </div>
                                    ))}
                                </>
                                }


                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
                
        </div>
       
    );
};

Font.register({ family: 'KadwaRegular', src:'https://fonts.gstatic.com/s/kadwa/v2/rnCm-x5V0g7ipiTAT8M.ttf' } );
Font.register({ family: 'KadwaBold', src:'https://fonts.gstatic.com/s/kadwa/v2/rnCr-x5V0g7ipix7atM5kn0.ttf' } );
Font.register({ family: 'MontserratRegular', src:'https://fonts.gstatic.com/s/montserrat/v12/JTUSjIg1_i6t8kCHKm459Wlhzg.ttf' } );

const styles = StyleSheet.create({
    bold900:{
        fontWeight:900,
        fontFamily:'KadwaBold'
    },
    contentCenter:{
        textAlign: 'center'
    },
    contentLeft:{
        fontSize:12,
        flex: 1,
        textAlign: 'left'
    },
    contentRight:{
        fontSize:12,
        flex: 1,
        textAlign: 'right'
    },
    marginTop30:{
        marginTop: 30
    },
    marginBottom30:{
        marginBottom: 30
    },
    page: { 
      backgroundColor: '#fff',
      padding:50
    },
    row:{
        display: 'flex',
        flexDirection: 'row',
    },
    sectionheader: { 
      color: 'black', 
      textAlign: 'center', 
      fontSize:14, 
      fontFamily:'KadwaBold',
      fontWeight:900,
      textTransform:'capitalize'
    },
    subsequentsectionheader: { 
      color: 'black', 
      textAlign: 'center', 
      marginTop: 30, 
      fontSize:14, 
      fontWeight: 'bold',
      textTransform:'capitalize'
    },
    sectioncontent: { 
      fontSize:12,
      fontFamily:'KadwaRegular'
    },
  	sectioncontent_left: { 
      color: 'black', 
      textAlign: 'left', 
      marginTop: 10, 
      fontSize:12,
      //textTransform:'capitalize'
    },
  	sectioncontent_right: { 
      color: 'black', 
      textAlign: 'right', 
      marginTop: -15, 
      fontSize:12,
      //textTransform:'capitalize'
    },
  	sectioncontent_right_city: { 
      color: 'black', 
      textAlign: 'right', 
      marginTop: 10, 
      fontSize:12,
      //textTransform:'capitalize'
    }
  });
  
    // Create Document Component
const ReactPDFDoc = (props) =>{
    
    const contact = props.resumeData.contact;
    const firstname = contact.firstname ? contact.firstname : "";
    const lastname = contact.lastname ? contact.lastname : "";
    const contactCity = contact.city ? contact.city : "";
    const contactState = contact.state ? contact.state : "";
    const contactZip = contact.zipcode ? contact.zipcode : "";

    let contactSection = `${contactZip} ${contactCity} ${contactState}`;
    
    if(contact.phone){
        contactSection=contactSection?contactSection+" | ":"";
        contactSection+="C: " + contact.phone;
    }
    if(contact.email){
        contactSection=contactSection?contactSection+" | ":"";
        contactSection+="E: " + contact.email;
    }
    if(contact.linkedIn){
        contactSection=contactSection?contactSection+" | ":"";
        contactSection+=contact.linkedIn;
    }

    const objective = props.resumeData.objective;
    const objectiveHeading = objective.heading ? objective.heading : "Summary";
    const objectiveSummary = objective.summary ? objective.summary : "";
    const objectiveObjective = objective.objective ? objective.objective : "";
    const objectiveProfileItems = objective.profileItems ? objective.profileItems : [];
    
    const educations = props.resumeData.educations ? props.resumeData.educations : [];

    const awardsHeading = props.resumeData.awards.heading ? props.resumeData.awards.heading : "Professional Organizations / Awards / Accomplishments";
    const awardsOrgs = props.resumeData.awards.organizations ? props.resumeData.awards.organizations : [];
    const awardsAwards = props.resumeData.awards.awards ? props.resumeData.awards.awards : [];

    const jobExperiences = props.resumeData.jobExperiences ? props.resumeData.jobExperiences : [];
    const moreJobExperiences = jobExperiences.length > 1 ? jobExperiences.slice(1) : [];
    
    return (
        <Document>
        <Page size="A4" style={styles.page}>
        <View wrap={false} style={styles.marginBottom30}>
            <Text style={styles.sectionheader}>{firstname} {lastname}</Text>
            <Text style={[styles.sectioncontent, styles.contentCenter]}>{contactSection}</Text>
            </View>
        <View wrap={false} style={styles.marginBottom30}>
            <Text style={[styles.sectionheader]}>{objectiveHeading}</Text>
            {objectiveHeading == "profile" &&
                objectiveProfileItems.map((item, index) => (
                    <Text key={"profile"+Math.random()} style={[styles.sectioncontent, styles.contentCenter]}>• {item}</Text>
                ))
            }
            {objectiveHeading == "summary" &&
                <Text style={[styles.sectioncontent, styles.contentCenter]}>{objectiveSummary}</Text>
            }
            {objectiveHeading == "objective" &&
                <Text style={[styles.sectioncontent, styles.contentCenter]}>{objectiveObjective}</Text>
            }
        </View>
        <View style={styles.marginBottom30}>
            { educations.length > 0 && <Text style={[styles.sectionheader]}>Education</Text>}
            { educations.length > 0 && educations.map((education, index) => (
                <View wrap={false} key={"education-"+Math.random()}>
                    <View style={styles.row} key={"education-"+Math.random()}>
                        <View style={styles.contentLeft}>
                            <Text style={[styles.sectioncontent, styles.bold900]}>{education.school}</Text>
                            <Text style={[styles.sectioncontent]}>{education.degree}, {education.studyField}</Text>
                        </View>
                        <View style={styles.contentRight}>
                            <Text style={[styles.sectioncontent]}>{education.city}{((education.city && education.state)?", ":"")}{education.state}</Text>
                            <Text style={[styles.sectioncontent]}>{education.graduateDate}</Text>
                        </View>
                    </View>
                    {education.classes.length > 0 &&
                        <>
                            <Text style={[styles.sectioncontent, {marginTop:5}]}>Coursework</Text>
                            {education.classes.map((schoolClass, index) => (
                                <Text key={"education-class-"+Math.random()} style={styles.sectioncontent}>• {schoolClass}</Text>
                            ))}
                        </>
                    }
                    <Text style={{marginBottom:10}}></Text>
                </View>
            ))}
        </View>
        <View wrap={false} style={styles.marginBottom30}>
            {(awardsOrgs.length > 0 || awardsAwards.length > 0) && <Text style={[styles.sectionheader]}>{awardsHeading}</Text>}
            {awardsHeading == "organizations" && awardsOrgs.length>0 &&
                awardsOrgs.map((org, index) => (
                    <Text key={"prof-org-"+Math.random()} style={styles.sectioncontent}>• {org}</Text>
                ))
            } 

            {awardsHeading == "awards / accomplishments" && awardsAwards.length>0 &&
                awardsAwards.map((award, index) => (
                    <Text key={"prof-org-"+Math.random()} style={styles.sectioncontent}>• {award}</Text>
                ))
            }
        </View>
        <View wrap={false}>
            {
                jobExperiences.length > 0 &&
                <>
                    <Text style={[styles.sectionheader]}>Job Experience</Text>
                    <View style={styles.row}>
                        <View style={styles.contentLeft}>
                            <Text style={[styles.sectioncontent]}>{jobExperiences[0].jobTitle}{((jobExperiences[0].jobTitle && jobExperiences[0].employer)?", ":"")}<Text style={styles.bold900}>{jobExperiences[0].employer}</Text></Text>
                        </View>
                        <View style={styles.contentRight}>
                            <Text style={[styles.sectioncontent]}>{jobExperiences[0].employmentStart}{((jobExperiences[0].employmentStart && jobExperiences[0].employmentEnd)?" to ":"")}{jobExperiences[0].employmentEnd}</Text>
                            <Text style={[styles.sectioncontent]}>{jobExperiences[0].city}{((jobExperiences[0].city && jobExperiences[0].state)?", ":"")}{jobExperiences[0].state}</Text>
                        </View>
                    </View>
                    <Text style={styles.sectioncontent}>{jobExperiences[0].description}</Text>
                    {jobExperiences[0].duties && jobExperiences[0].duties.map((duty, index) => (
                            <Text key={"duty-"+Math.random()} style={styles.sectioncontent}>• {duty}</Text>
                     ))}
                     <Text style={{marginBottom:20}}></Text>
                </>
            }
        </View>
        <View wrap={false}>
            {
                moreJobExperiences.length > 0 &&
                moreJobExperiences.map((experience, index) => (
                    <>
                        <View wrap={false} key={"duty-"+Math.random()}>
                            <View style={styles.row}>
                                <View style={styles.contentLeft}>
                                    <Text style={[styles.sectioncontent]}>{experience.jobTitle}{((experience.jobTitle && experience.employer)?", ":"")}<Text style={styles.bold900}>{experience.employer}</Text></Text>
                                </View>
                                <View style={styles.contentRight}>
                                    <Text style={[styles.sectioncontent]}>{experience.employmentStart}{((experience.employmentStart && experience.employmentEnd)?" to ":"")}{experience.employmentEnd}</Text>
                                    <Text style={[styles.sectioncontent]}>{experience.city}{((experience.city && experience.state)?", ":"")}{experience.state}</Text>
                                </View>
                            </View>
                            <Text style={styles.sectioncontent}>{experience.description}</Text>
                            {experience.duties && experience.duties.map((duty, index) => (
                                    <Text key={"duty-"+Math.random()} style={styles.sectioncontent}>• {duty}</Text>
                            ))}
                            <Text style={{marginBottom:20}}></Text>
                        </View>
                    </>
                ))
            }
        </View>
        </Page>
      </Document>
        )
}

// const Container = () => {
//     return (
//         <div id="react-pdf-container">

//         </div>
//     );
// }

export default Template1;
export const PDFDoc = ReactPDFDoc;
// export const PDFDocRenderContainer = Container;