import {defaultResume, defaultCoverLetter1} from '../services/persistence';

export function finalizedResumeFromSource(loadedResume){
    var resumeData = defaultResume();
    if(loadedResume.hasOwnProperty('data') && loadedResume.data.length > 0){
        if(!loadedResume.data[0].resume_content || loadedResume.data[0].resume_content === ""){
            resumeData = defaultResume();
        }
        else{
            const loadedData = JSON.parse(loadedResume.data[0].resume_content);
            resumeData = loadedData;
        }
    }
    else if(loadedResume.hasOwnProperty('data') && loadedResume.data.length === 0){
        resumeData = defaultResume();
    }
    else{
        resumeData = loadedResume;
    }
    return resumeData;
}

export function finalizedCoverLetter1FromSource(loadedCoverLetter){
    var coverLetter1Data = defaultCoverLetter1();
    if(loadedCoverLetter.hasOwnProperty('data') && loadedCoverLetter.data.length > 0){
        if(!loadedCoverLetter.data[0].content || loadedCoverLetter.data[0].content === ""){
            coverLetter1Data = defaultCoverLetter1();
        }
        else{
            const loadedData = JSON.parse(loadedCoverLetter.data[0].content);
            coverLetter1Data = loadedData;
        }
    }
    else if(loadedCoverLetter.hasOwnProperty('data') && loadedCoverLetter.data.length === 0){
        coverLetter1Data = defaultCoverLetter1();
    }
    else{
        coverLetter1Data = loadedCoverLetter;
    }

    if(!coverLetter1Data.heading){
        coverLetter1Data.heading = {};
    }

    return coverLetter1Data;
}

const formatCoverLetterHeadingDate = (date) => {
    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ]; 
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
  
    return monthNames[monthIndex] + ' ' + day + ', ' + year;
  }